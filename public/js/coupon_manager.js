$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();

    //filterDialog categories checkboxes
    $('#filterDialog input[type="checkbox"]').change(function () {
        if ($(this).prop("checked")) {
                $(this).prev().addClass('active');
            return;
        }
        else {
            $(this).prev().removeClass('active');
        }
    });

   //AJAX REQUEST

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('a:not([data-toggle]):not([href="#"]):not([no-loader]):not(.page-link)').on('click', function() {
    $('.loader').addClass('loading');
})

setTimeout(function() {
    $('.loader').removeClass('loading');
}, 1500);

/* form submit */
$(document).on('submit', 'form[data-ajax]', function(e) {
    e.preventDefault();
    var trigger = $(this);
    var action = trigger.attr('action');
    var method = trigger.attr('method');
    var data = trigger.serialize();
    ajaxCall(trigger, action, method, data);
});

/* a/button click */
$(document).on('click', 'a[data-ajax], button[data-ajax]', function() {
    var trigger = $(this);
    var action = trigger.data('action');
    var method = trigger.data('method');
    var data = trigger.data('request');
    if(method === "delete"){
        swal({
            title: 'Sei sicuro?',
            text: "L'operazione è irreversibile",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#243A7E',
            cancelButtonColor: '#CCCCCC',
            confirmButtonText: 'Elimina',
            reverseButtons: true,
        }).then(function(response){
            if (!response.dismiss) {
                ajaxCall(trigger, action, method, data);
                swal({
                    title: 'Operazione completata!',
                    text: "Eliminazione effettuata con successo!",
                    type: 'success',
                    showConfirmButton: false,
                    showCancelButton: true,
                    cancelButtonColor: '#243A7E',
                    cancelButtonText: 'Ok'
                })
            }
        })
    } else {
        ajaxCall(trigger, action, method, data);
    }
});

/* form > input on change */
$(document).on('change', 'form.filter[data-ajax] input', function() {
    var trigger = $(this).parents('form[data-ajax]');
    var action = trigger.attr('action');
    var method = trigger.attr('method');
    var data = trigger.serialize();
    ajaxCall(trigger, action, method, data);
});

/* pagination link click */
$(document).on('click', '.ajax_paginate[data-ajax] a', function(e) {
    e.preventDefault();
    var trigger = $(this).parents('.ajax_paginate[data-ajax]');
    var action = $(this).attr('href');
    var method = 'get';
    ajaxCall(trigger, action, method);
    return false;
});

$(document).on('keyup', 'input[data-ajax]', function() {
    var trigger = $(this);
    var action = trigger.data('action');
    var method = 'get';
    var data = trigger.serialize();
    ajaxCall(trigger, action, method, data);
});

function ajaxCall(trigger, action, method, data = null) {
    var $spinnerContainer = $("<div>", {"class": "spinner-container d-flex justify-content-center align-items-center flex-column pb-3"});
    let spinner = '<svg class="spinner position-static" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">';
    spinner += '<circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>';
    spinner += '</svg>';
    $spinnerContainer.append(spinner);
    if(trigger.data('sending-email') != null) {
        $('.spinner').addClass('mb-4');
        $spinnerContainer.append('<span style="color:#ccc; font-size:16px;">Stiamo notificando il cliente...</span>');
    }
    $(trigger.data('ajax')).html($spinnerContainer);
    if (trigger.parents('.modal').length) {
        $('#loader').removeClass('hide');
    }
    if(trigger.data('prize-request')) {
        $('#requestBtn:not(.disabled)').addClass('disabled').html('<i class="fa fa-spinner fa-spin mr-2"></i> Stiamo elaborando la sua richiesta...');
    }
    //REFER
    $.ajax({
        url: action,
        type: method,
        data: data
    }).done(function (response) {
        console.log(response);
        $('#loader').addClass('hide');
        if (response.tipo == "danger") {
            for (msg in response.messaggio) {
                console.log(response.titolo, response.messaggio[msg], response.tipo);
            }
        }
        else {

            if(response.tipo && response.tipo === 'table') {
                return setTimeout(function() {
                    $(trigger.data('ajax')).html(response.risultato);
                },500);
            }

            if(response.tipo === 'alert') {
                $('#requestBtn.disabled').html('<i class="fa fa-check mr-2"></i> Premio richiesto');
                var htmlText = response.messaggio;
                return Swal.fire({
                        title: 'Complimenti!',
                        html: htmlText,
                        type: 'success',
                        showConfirmButton: false,
                        showCancelButton: true,
                        cancelButtonText: 'Chiudi'
                    })

            }

            $(trigger.data('ajax')).html(response.risultato);

            if(response.messaggio) {
                $('#success-alert').find('#success').html(response.messaggio);
                $('#success-alert').fadeIn();
                setTimeout(function() {
                    $('#success-alert').fadeOut();
                }, 2000);
            }

            $('#cards-accordion .card-header a').on('click', function() {
                if($(this).parent().parent().hasClass('open')){
                    $(this).parent().parent().removeClass('open');
                }else {
                    $(this).parent().parent().addClass('open');
                }
            });

            if($('body').hasClass('cards_mobile')){
                $('.card-product').each(function(i){
                    $(this).addClass('bounceIn');
                });
            }
            $('select').selectpicker();
        }

    }).fail(function (response) {
        console.log(response);
    });
}

/*modal reset on close*/
$('.modal').on('hidden.bs.modal', function (event) {
    $(this).find('form').trigger('reset');
});
//modal on show check categories
$('.modal').on('show.bs.modal', function(){
    $(this).find('label.active').trigger('click');
});
//AJAX REQUEST


//CATEGORIES FILTER CHECKBOXES
function checkboxes(){
    var counter = 0;
    $('.nav-pills .nav-item').removeClass('ml-0');
    $('.nav-pills .nav-item').each(function(i) {
        if(i > 3){
            $(this).parent().addClass('d-flex justify-content-center');
        }
        if($(window).width() > 479){
            if (i % 4 == 0) {
                counter = 1;
            } else {
                counter++;
            }
        }
        else {
            if (i % 3 == 0) {
                counter = 1;
            } else {
                counter++;
            }
        }
        if (counter == 1) {
            $(this).addClass('ml-0');
        }
    });
}

function isTouchDevice(){
    return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
}

if(isTouchDevice()===false) {
    $("[rel='tooltip']").tooltip();
}

checkboxes();

$('.updateOpVal').on('click', function () {
    var icon = $(this).children('i');
   $(this).children('i').addClass('fa-spin');
   var target = $(this).data('target');
   var value = $('#' + target).val();
   $.ajax({
       type: "PUT",
       data: {id: target, value: value},
       url: 'operations/' + target,
    })
   .done( function(response) {
       console.log(response);
       icon.removeClass('fa-spin');
       icon.text('check');
       setTimeout(function(){
           icon.text('sync');
       }, 2000);
   })
   .fail( function (response) {
       console.log(response);
   })
});

$(window).on('resize', function(){
    checkboxes();
});

//FORM VALIDATIONS

    //CUSTOMER ADD
    var customerValidator = $('#customer').validate({
        rules: {
            name: "required",
            surname:"required",
            city: "required",
            email: "email",
            username: "required",
            password: "required",
            password_confirmation: {
                required: true,
                equalTo: '#input-password'
            },
            card: "required"
        },
        messages: {
            name: "Il campo nome è richiesto.",
            surname: "Il campo cognome è richiesto.",
            city: "Il campo provincia è richiesto.",
            email: {
                email: "Il campo non contiene un indirizzo email valido."
            },
            username: "Lo username è richiesto.",
            password: "La password è richiesta.",
            password_confirmation: {
                required: "Inserisci di nuovo la password",
                equalTo: 'Le due password inserite devono essere identiche.'
            },
            card: "Il numero card è richiesto."
        }
    });

    $('form[validate] input[type="submit"]').on('click', function() {
        customerValidator.form();
    });

    //CUSTOMER EDIT
    var customerEdit = $('#customer_edit').validate({
        rules: {
            name: "required",
            surname:"required",
            city: "required",
            email: "email",
            username: "required",
            password: "required",
            password_confirmation: {
                required: true,
                equalTo: '#input-password'
            }
        },
        messages: {
            name: "Il campo nome è richiesto.",
            surname: "Il campo cognome è richiesto.",
            city: "Il campo provincia è richiesto.",
            email: {
                email: "Il campo non contiene un indirizzo email valido."
            },
            username: "Lo username è richiesto.",
            password: "La password è richiesta.",
            password_confirmation: {
                required: "Inserisci di nuovo la password",
                equalTo: 'Le due password inserite devono essere identiche.'
            }
        }
    });

    $('form[validate] input[type="submit"]').on('click', function() {
        customerEdit.form();
    });

    //CARD STORE
    var cardForm = $('#card-form').validate({
        rules: {
            card: "required"
        },
        messages: {
            card: "Il numero card è richiesto."
        }
    });

    $('form[validate] input[type="submit"]').on('click', function() {
        cardForm.form();
    });

    //END FORM VALIDATION

    // PRIZE REQUEST

    $('.card-box').on('click', function() {
        var ref = $(this);
            $('.card-box').fadeOut();
            $('.card-action-box #card-number').html(ref.attr('data-number'));
            $('.card-action-box #card-points').html('Saldo: ' + ref.attr('data-points') + ' punti');
            $('.card-action-box #doAction').attr('data-card', ref.attr('data-number'));
            setTimeout(function() {
                $('#goBackToCards').fadeIn();
                $('.card-action-box-wrapper').fadeIn();

                // binding input event listener
                $('.card-action-box input').on('keyup', function() {
                    var value = $(this).val();
                    var inputGroup = $('.card-action-box .input-group');
                    var prizeValue = $('#prizeValue').attr('data-value');

                    if(value == '' || value == '0' || parseInt(value) > ref.attr('data-points') || parseInt(value) > prizeValue) {

                        inputGroup.addClass('has-danger');

                        // cases
                        (parseInt(value) > ref.attr('data-points')) ? inputGroup.addClass('excedeed') : inputGroup.removeClass('excedeed');
                        (parseInt(value) > prizeValue) ? inputGroup.addClass('excedeed-prize-value') : inputGroup.removeClass('excedeed-prize-value');
                        value == '0' ? inputGroup.addClass('zero') : inputGroup.removeClass('zero');

                        if(!$('#doAction').hasClass('disabled')) {
                            $('#doAction').addClass('disabled');
                        }
                    } else {
                        inputGroup.removeClass('has-danger');
                        $('#doAction').removeClass('disabled');
                    }
                });

            }, 1000);
    });

    $('#goBackToCards').on('click', function() {
        goBackToCards();
    });

    var initialData = [];
    var init = false;

    //on modal show event
    $('#prizeRequestModal').on('show.bs.modal', function () {

        if(!init) {
            $('.card-box').each( function() {
                initialData.push({
                    number: $(this).attr('data-number'),
                    points: $(this).attr('data-points')
                })
            });
            init = true;
        }

        //binding event listeners
        $(document).keypress(function(event){
            var value = $('.card-action-box input').val();
            var number = $('#doAction').attr('data-card');
            var rel = $('.card-box[data-number="' + number + '"]');
            var prizeValue = $('#prizeValue').attr('data-value');

            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13' && value !== '' && value !== 0 && parseInt(value) <= rel.attr('data-points') && parseInt(value) <= prizeValue) {
                $('#doAction').trigger('click');
            }
        });
    });

    //on modal dismiss action

    $('#prizeRequestModal').on('hidden.bs.modal', function () {
        goBackToCards();

    });

    $('#doAction').on('click', function() {
        var cardNumber = 0;
        var relCard = {};
        var newPoints = 0;
        var prizeValue = 0;

        cardNumber = $(this).attr('data-card');

        // setting new data attributes

        relCard    = $('.card-box[data-number="' + cardNumber + '"]');
        newPoints  = relCard.attr('data-points') - $('.card-action-box input').val();
        prizeValue = $('#prizeValue').attr('data-value');

        relCard.attr('data-points', newPoints);
        $('#prizeValue').attr('data-value', prizeValue - $('.card-action-box input').val());
        $('#prizeValue').html(prizeValue - $('.card-action-box input').val());
        prizeValue -= $('.card-action-box input').val();
        $('.card-action-box input').val("");

        // all prize value covered
        if(prizeValue == 0) {
            relCard.attr('data-transaction', true);
            $('.cards-container').fadeOut();
            $('#prizeValue').css('font-size', '38px');
            $('#prizeValue').html('Conferma la richiesta');
            $('.prize-points span:nth-child(2)').html('cliccando sul pulsante qui in basso');
            $('#confirm').removeClass('disabled');
            var cardsUpdates = [];
            $('.card-box').each(function() {
               cardsUpdates.push({
                       'number': $(this).attr('data-number'),
                       'points': $(this).attr('data-points'),
                       'dataTransaction': $(this).attr('data-transaction')
                   });
            });
            $('#confirm').on('click', function () {
                cardsUpdates = cardsUpdates.filter(item => item.dataTransaction == "true");
                $.ajax({
                    url: $(this).data('action'),
                    type: 'POST',
                    data: {data: cardsUpdates},
                }).done(function(response) {
                    console.log(response);
                    if(response.tipo === 'alert') {
                        $('#prizeRequestModal').modal('hide');
                        $('#requestBtn:not(.disabled)').addClass('disabled').html('<i class="fa fa-check mr-2"></i> Premio richiesto');
                        $('.user-points p').html('<strong>' + response.userPoints + '</strong>' + ' Punti');
                        return Swal.fire({
                            title: 'Complimenti!',
                            text: 'Premio richiesto con successo',
                            type: 'success',
                            showConfirmButton: false,
                            showCancelButton: true,
                            cancelButtonText: 'Chiudi'
                        })

                    }

                    if(response.messaggio) {
                        $('#success-alert').find('#success').html(response.messaggio);
                        $('#success-alert').fadeIn();
                    }
                })
                .fail(function (response) {
                    console.log(response);
                    return Swal.fire({
                        title: 'Errore con la richiesta!',
                        text: 'Prova a ricaricare la pagina e ripetere l\'operazione.',
                        type: 'error',
                        icon: 'error',
                        showConfirmButton: false,
                        showCancelButton: true,
                        cancelButtonText: 'Chiudi'
                    })
                });
            });
        } else {

            $('#' + cardNumber +'-points').html('<strong>' + newPoints + '</strong>');

            if(newPoints === 0) {
                relCard.addClass('disabled');
            }

            goBackToCards();
        }

    });


    function goBackToCards() {
        $('.card-action-box-wrapper').fadeOut();
        $('#goBackToCards').fadeOut();
        setTimeout(function() {
            $('.card-box').fadeIn();
        }, 1000);
    }



});