<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

//AUTH VIEWS
Route::group(['middleware' => 'auth'], function () {

    //SEARCH ROUTES
    Route::get('coupon/search', 'CouponController@search')->name('coupon.search');
    Route::get('customers/search', 'CustomerController@search')->name('customers.search');
    Route::get('customers/search-from-home', 'CustomerController@searchFromHome')->name('customers.search-from-home');

	Route::resource('user', 'UserController');
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	Route::post('crop-image', ['as'=>'upload-image','uses'=>'UserController@imageCrop']);
    Route::post('crop-image-coupon', ['as'=>'upload-image-coupon','uses'=>'CouponController@imageCrop']);
	Route::resource('coupon', 'CouponController');
	Route::resource('category', 'CouponCategoryController', ['except' => ['show', 'search']]);
	Route::resource('category', 'CouponCategoryController', ['except' => ['show', 'search']]);
    Route::resource('customers', 'CustomerController');
    Route::resource('operations', 'OperationController', ['except' => ['show, create, destroy']]);
    Route::resource('card', 'CardController', ['except' => ['show', 'create', 'edit']]);
    Route::resource('movement', 'MovementController', ['except' => ['show', 'create', 'edit']]);

    Route::get('customers/{customer}/cards', 'CustomerController@cards')->name('customers.cards');
    Route::put('customers/{customer}/password', 'CustomerController@password')->name('customers.password');

    //PRIZE VALIDATION
    Route::put('validate/{user}/{prize}', 'HomeController@validatePrize')->name('validate-prize');

    //FRONTEND ROUTES
    Route::get('premi/{slug}', 'FrontendController@showCoupon')->name('frontend.show-coupon');
    Route::post('{coupon}/richiesta-premio', 'FrontendController@requestPrize')->name('frontend.request-prize');
    Route::get('movimenti', 'FrontendController@allMovements')->name('frontend.movements');
    Route::get('richiesta-premio/{slug}', 'FrontendController@prizeStatus')->name('frontend.prize-status');

});

Route::get('links/{slug}', 'GuestController@showCoupon')->name('guest.show-coupon');