import swiper from 'swiper/bundle';
import 'swiper/swiper-bundle.css';
import ClipboardJS from 'clipboard';

const clipboard = new ClipboardJS('.clipboard');

// CUSTOMER VIEW
const mySwiper = new swiper(
    '.swiper-container', {
        autoplay: false,
        centeredSlides: false,
        loop: false,
        observeParents: true,
        observer: true,
        preventClicks: false,
        pagination: {
            el: 'div.swiper-pagination.swiper-pagination-clickable.swiper-pagination-bullets',
            type: "bullets",
            clickable: true
        },
        slidesPerView: "auto",
        grabCursor: true,
        breakpoints: {
            991: {
                navigation: {
                    nextEl: '.arrow-right',
                    prevEl: '.arrow-left'
                }
            }
        }
    }
);