@extends('layouts.app', ['activePage' => 'operations-management', 'title' => __('Gestione Operazioni')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('Operazioni') }}</h4>
                <p class="card-category"> {{ __('Qui puoi gestire il valore delle operazioni della piattaforma') }}</p>
              </div>
              <div class="card-body">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>
                          {{ __('Operazione') }}
                      </th>
                      <th>
                        {{ __('Punti per euro') }}
                      </th>
                    </thead>
                    <tbody>
                      @foreach($operations as $operation)
                        @if($operation->name !== 'RICHIESTA PREMIO')
                        <tr>
                          <td>
                            {{ $operation->name }}
                          </td>
                          <td>
                            <form>
                              <div class="form-group">
                                <input type="number" class="form-control float-left opInput" id="{{ $operation->id }}" aria-describedby="helpId" value="{{ $operation->ppeuro }}">
                                <span class="updateOpVal float-left d-inline-block" data-target="{{ $operation->id }}"><i class="material-icons">sync</i></span>
                              </div>
                            </form>
                          </td>
                        </tr>
                        @endif
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection