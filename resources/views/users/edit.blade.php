@extends('layouts.app', ['activePage' => 'user-management', 'title' => __('Modifica Utente')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('user.update', $user) }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('PUT')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Modifica Utente') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                    <a href="{{ route('user.index') }}" class="btn btn-sm btn-primary">{{ __('Torna alla lista') }}</a>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nome') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Inserisci nome') }}" value="{{ $user->name }}" required="true" aria-required="true"/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Cognome') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('surname') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" id="input-surname" type="text" placeholder="{{ __('Inserisci cognome') }}" value="{{ $user->surname }}" required />
                      @if ($errors->has('surname'))
                        <span id="surname-error" class="error text-danger" for="input-surname">{{ $errors->first('surname') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Email') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="{{ __('Inserisci email') }}" value="{{ $user->email }}" />
                      @if ($errors->has('email'))
                        <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Username') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('username') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" id="input-username" type="text" placeholder="{{ __('Inserisci uno username') }}" value="{{ $user->username }}" required />
                      @if ($errors->has('username'))
                        <span id="username-error" class="error text-danger" for="input-surname">{{ $errors->first('username') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Hotel') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('company') ? ' has-danger' : '' }}">
                      <select name="company" class="selectpicker"
                              data-style="btn btn-primary btn-round" data-size="7">
                        <option value="Hotel Promenade" @if($user->company == 'Hotel Promenade')  selected @endif>Hotel Promenade</option>
                        <option value="Hotel Villa Medici" @if($user->company == 'Hotel Villa Medici')  selected @endif>Hotel Villa Medici</option>
                      </select>
                      @if ($errors->has('company'))
                        <span id="company-error" class="error text-danger" for="input-company">{{ $errors->first('company') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <hr class="mx-auto mb-4" style="max-width:900px">
                <div class="row">
                  <div class="col-12 text-center">
                    <h4 class="card-title float-none font-weight-normal">Modifica password</h4>
                    <p class="card-category mt-1 float-none">
                      Inserisci la nuova password per l'utente
                    </p>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-password">{{ __(' Password') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" input type="password" name="password" id="input-password" placeholder="{{ __('Inserisci password') }}" value="" />
                      @if ($errors->has('password'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('password') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-password-confirmation">{{ __('Conferma Password') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input class="form-control" name="password_confirmation" id="input-password-confirmation" type="password" placeholder="{{ __('Conferma Password') }}" value="" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Modifica utente') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection