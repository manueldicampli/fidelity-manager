<div class="modal-header">
    <div class="user mb-3">
        <div class="photo mr-3 mb-3 mb-md-0">
            <img src="@if($user->avatar == ''){{ asset('img/users/user.jpg')}}@else{{ asset('storage/images/users/'.$user->avatar)}}@endif">
        </div>
        <div class="user-info py-1">
            <h4>{{ $user->name.' '.$user->surname }}</h4>
            <small>{{$user->company}}</small>
        </div>
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body pt-0">
    <div class="row">
        <div class="col-12 info-box">
            <h4>Email</h4>
            <a href="mailto:{{$user->email}}">{{$user->email}}</a>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary close-btn" data-dismiss="modal">Chiudi</button>
</div>