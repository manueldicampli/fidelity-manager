@extends('layouts.app', ['activePage' => 'user-management', 'title' => __('Gestione Utenti')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">{{ __('Lista utenti') }}</h4>
                            <p class="card-category"> {{ __('Qui puoi gestire gli utenti della piattaforma') }}</p>
                        </div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="alert alert-success">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <i class="material-icons">close</i>
                                            </button>
                                            <span>{{ session('status') }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{ route('user.create') }}"
                                       class="btn btn-sm btn-primary">{{ __('Aggiungi utente') }}</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                    <th>
                                        {{ __('Nome') }}
                                    </th>
                                    <th>
                                        {{ __('Cognome') }}
                                    </th>
                                    <th>
                                        {{ __('Hotel') }}
                                    </th>
                                    <th class="text-right">
                                        {{ __('Azioni') }}
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>
                                                {{ $user->name }}
                                            </td>
                                            <td>
                                                {{ $user->surname }}
                                            </td>
                                            <td>
                                                {{ $user->company }}
                                            </td>
                                            <td class="td-actions text-right">
                                                @if ($user->id != auth()->id())
                                                        <button class="btn btn-primary btn-link" style="background: none !important; box-shadow: none!important;" href="#" data-toggle="modal" data-target="#modelId" data-ajax="#modal_content" data-method="get" data-action="{{ route('user.show', $user)}}">
                                                            <i class="material-icons">remove_red_eye</i>
                                                        </button>
                                                        <a class="btn btn-success btn-link"
                                                           href="{{ route('user.edit', $user) }}">
                                                            <i class="material-icons">edit</i>
                                                        </a>
                                                    <form action="{{ route('user.destroy', $user) }}" method="post" class="d-inline-block">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="button" class="btn btn-danger btn-link"
                                                                onclick="confirm('{{ __("Sei sicuro di voler eliminare questo utente?") }}') ? this.parentElement.submit() : ''">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </form>
                                                @else
                                                    <button class="btn btn-primary btn-link" style="background: none !important; box-shadow: none!important;" href="#" data-toggle="modal" data-target="#modelId" data-ajax="#modal_content" data-method="get" data-action="{{ route('user.show', $user)}}">
                                                        <i class="material-icons">remove_red_eye</i>
                                                    </button>
                                                    <a class="btn btn-success btn-link"
                                                       href="{{ route('profile.edit') }}">
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div id="modal_content">
                    @isset($customer)
                        @include('users.show')
                    @endisset
                </div>
            </div>
        </div>
    </div>
@endsection