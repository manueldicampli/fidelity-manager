<div class="table-responsive">
    <table class="table">
        <thead class=" text-primary">
        <tr>
            <th>{{ __('Data') }}</th>
            <th>{{__('Cliente')}}</th>
            <th>{{ __('Tipologia') }}</th>
            <th>{{ __('Importo') }}</th>
            <th>{{__('Operatore')}}</th>
            <th>{{__('Hotel')}}</th>
            <th class="text-right">{{ __('Azioni') }}</th>
        </thead>
        <tbody>
        @forelse($movements as $movement)
            <tr>
                <td>{{\Carbon\Carbon::parse($movement->created_at)->format('d/m/Y')}}</td>
                <td>{{ $movement->card->user->name.' '.$movement->card->user->surname.' - '.$movement->card->number  }}</td>
                <td>@if($movement->partial)(PAR) @endif {{ $movement->operation->name}} @if($movement->operation->id == 5) : {{\Illuminate\Support\Str::limit($movement->coupon->title, 15, $end='...') }} @endif</td>
                <td><i
                            class="material-icons align-middle {{($movement->type === 'up') ? 'movement-up' : 'movement-down' }}">{{($movement->type === 'up') ? 'arrow_drop_up' : 'arrow_drop_down' }}</i>{{ (($movement->type === 'up') ? '+' : '-').$movement->amount }} punti
                </td>
                <td>@if($movement->operation->id == 5)Cliente @else {{ $movement->user->name.' '.$movement->user->surname }} @endif</td>
                <td>{{ $movement->location }}</td>
                <td class="td-actions text-right">
                        <form action="{{ route('movement.destroy', $movement) }}" method="post" class="d-inline-block">
                            @csrf
                            @method('delete')
                            <button type="button" class="btn btn-danger btn-link" onclick="confirm('{{ __("Sei sicuro di voler eliminare questo movimento?") }}') ? this.parentElement.submit() : ''">
                                <i class="material-icons">close</i>
                            </button>
                        </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="6"><p id="no-movements" class="text-center font-weight-bold"><i
                                class="material-icons align-bottom">notification_important</i> Non è stato effettuato ancora nessun movimento</p></td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>