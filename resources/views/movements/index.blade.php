@extends('layouts.app', ['activePage' => 'movements-management', 'title' => __('Movimenti')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary d-flex justify-content-between align-items-center">
                            <div class="title my-0">
                                <h4 class="card-title ">{{ __('Lista Movimenti') }}</h4>
                                <p class="card-category"> {{ __('Qui puoi visualizzare tutti i movimenti effettuati') }}</p>
                            </div>
                            {{--<div class="movements-search">
                                <input class="form-control" type="text" placeholder="Nome, cognome..." name="search" data-ajax="#movements_table" data-action="{{route('movements.search')}}" data-method="get">
                                <i class="material-icons">search</i>
                            </div>--}}
                        </div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="alert alert-success">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <i class="material-icons">close</i>
                                            </button>
                                            <span>{{ session('status') }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div id="movements_table">
                                @include('movements.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection