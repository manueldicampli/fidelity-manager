@extends('layouts.app', ['activePage' => 'home', 'title' => 'Home'])

<style>
    .swal2-popup .swal2-styled {
        border-radius:999px!important;
    }
    .swal2-popup .swal2-styled.swal2-confirm {
        background: #d29836 !important;
        box-shadow: 0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px #d2983663 !important;
    }
</style>

@section('content')
    @if(Auth::user()->isAdmin())
        <div class="content admin">
            <div class="profile-name">
                <h3 class="mb-0">Bentornato,</h3>
                <h2>{{ auth()->user()->name.' '.auth()->user()->surname }}</h2>
            </div>
            <div class="container-fluid ml-0" style="max-width:991px">
                <div class="row">
                    <div class="col-12">
                        <div class="card d-flex flex-md-row align-items-center justify-content-md-between">
                            <div class="card-header">
                                <h4 class="card-title"><i class="material-icons mr-2 align-middle">search</i>Cerca
                                    cliente</h4>
                            </div>
                            <div class="card-body d-flex flex-row align-items-center justify-content-md-end">
                                <form method="GET" action="{{route('customers.search-from-home')}}"
                                      class="d-flex align-items-center">
                                    @csrf
                                    <div class="customer-search border mr-3" style="height: 38px;">
                                        <input class="form-control" type="text" placeholder="Nome / Cognome / N.Card"
                                               name="search">
                                        <i class="material-icons">search</i>
                                    </div>
                                    <input type="submit" class="btn btn-primary btn-round" value="CERCA"/>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mt-0 flex-row align-items-center justify-content-between">
                            <div class="card-header">
                                <h4 class="card-title"><i class="material-icons mr-2 align-middle">person</i>Aggiungi
                                    cliente</h4>
                            </div>
                            <div class="card-footer">
                                <a href="{{route('customers.create')}}" class="btn btn-primary btn-round w-100">AGGIUNGI
                                    NUOVO</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mt-0 flex-row align-items-center justify-content-between">
                            <div class="card-header">
                                <h4 class="card-title"><i class="material-icons mr-2 align-middle">local_offer</i>Crea
                                    premio</h4>
                            </div>
                            <div class="card-footer">
                                <a href="{{route('coupon.create')}}" class="btn btn-primary btn-round w-100">CREA
                                    NUOVO</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header card-header-primary d-flex justify-content-between align-items-center"
                                 style="background-color: #d29836!important;">
                                <div class="title my-0">
                                    <h4 class="card-title ">{{ __('Richieste premio') }}</h4>
                                    <p class="card-category"> {{ __('Negli ultimi 7 giorni') }}</p>
                                </div>
                            </div>
                            <div class="card-body movements-details pb-0">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Data</th>
                                        <th>Nome</th>
                                        <th>Cliente</th>
                                        <th>Validazione</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($movements as $movement)
                                        <tr>
                                            <td>{{\Carbon\Carbon::parse($movement->created_at)->format('d/m/Y')}}</td>
                                            <td>{{ $movement->coupon->title }}</td>
                                            <td>{{ ($movement->card->user->name)[0].'. '.$movement->card->user->surname.' - #'. $movement->card->number }}</td>
                                            <td id="validation-{{$movement->id}}">
                                                @if($movement->card->user->CouponPivot($movement->coupon->id)->accepted)
                                                    Validato: <strong>{{ $movement->card->user->CouponPivot($movement->coupon->id)->accepted }}</strong>
                                                @else

                                                    <a href="javascript:void(0)" class="btn btn-primary btn-round"
                                                       id="validate-prize"
                                                       no-loader
                                                       data-prize-name="{{ $movement->coupon->title }}"
                                                       data-movement="{{$movement->id}}"
                                                       data-customer-name="{{ ($movement->card->user->name)[0].'. '.$movement->card->user->surname.' - #'. $movement->card->number }}"
                                                       data-url="{{ route('validate-prize', ['user' => $movement->card->user->id, 'prize' => $movement->coupon->id]) }}">Valida</a>
                                            </td>
                                            @endif
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center font-weight-bold animated fadeIn"><i
                                                            class="material-icons align-bottom">notification_important</i>Nessuna
                                                    richiesta</p>
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div id="modal_content">
                        @isset($customer)
                            @include('modal')
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="container py-5">

            <div class="d-sm-flex mb-5">
                <div class="profile-info mb-4">

                    <div class="profile-photo shadow"
                         style="background-image: url('{{ auth()->user()->avatar != '' ? asset('storage/images/users/'.auth()->user()->avatar) : asset('img/users/user.jpg')}}')"
                         data-toggle="modal"
                         data-target="#upload_modal">
                    </div>
                    <div class="profile-name">
                        <h3>Bentornato,</h3>
                        <h2>{{ auth()->user()->name.' '.auth()->user()->surname }}</h2>
                    </div>
                </div>
                <div class="user-points shadow rounded ml-auto my-auto p-2">
                    <i class="material-icons d-inline-block">star</i>
                    <p class="d-inline-block"><strong>{{ $user_points }}</strong> Punti</p>
                </div>
            </div>

            <section id="movements">
                <div class="movements-heading heading d-sm-flex">
                    <h3 class="my-0">I tuoi movimenti</h3>
                    @if(count($movements) > 0)
                    <a href="{{ route('frontend.movements') }}" class="all-movements d-inline-block ml-auto"
                       style="line-height: 37px;"><i
                                class="material-icons">search</i> Visualizza tutti i movimenti</a>
                    @endif
                </div>
                <ul class="movements-list">
                    @forelse($movements as $movement)
                        <li class="movements-item shadow rounded">
                            <div class="d-block d-sm-flex px-3 @if($movement->operation->id == 5) align-items-center py-3 @else py-4 @endif">
                                <div class="movements-item-date movements-item-meta mr-3 mb-3 mb-md-0">
                                    <i class="material-icons">calendar_today</i>
                                    <span>{{\Carbon\Carbon::parse($movement->created_at)->format('d/m/Y')}}</span>
                                </div>
                                <div class="movements-item-operation-name movements-item-meta mr-3 mb-3 mb-md-0">
                                    <i class="material-icons">
                                        @switch($movement->operation->name)
                                            @case('RISTORANTE')
                                            restaurant
                                            @break
                                            @case('BENESSERE E RELAX')
                                            spa
                                            @break
                                            @case('BAR')
                                            local_cafe
                                            @break
                                            @case('PERNOTTAMENTO')
                                            king_bed
                                            @break
                                            @default
                                            category
                                        @endswitch
                                    </i>
                                    <span>@if($movement->partial)
                                            (PAR) @endif{{ $movement->operation->name}} @if($movement->operation->id == 5)
                                            : {{\Illuminate\Support\Str::limit($movement->coupon->title, 15, $end='...') }} @endif</span>
                                </div>
                                <div class="movements-item-location movements-item-meta mr-3 mb-3 mb-md-0">
                                    <i class="material-icons">location_on</i>
                                    <span>{{$movement->location}}</span>
                                </div>
                                @if($movement->operation->id == 5)
                                <div class="d-none d-md-block movements-item-location movements-item-meta mr-3 mb-3 mb-md-0 btn btn-primary btn-fab btn-round ml-auto">
                                    <a class="d-block h-100" data-toggle="tooltip" data-trigger="hover" style="pointer-events:all" title="Visualizza richiesta" href="{{ route('frontend.prize-status', $movement->coupon->slug) }}"><i class="material-icons" style="color:#FFF!important; font-size:12px">remove_red_eye</i></a>
                                </div>
                                <div class="d-block d-md-none movements-item-location movements-item-meta mr-3 mb-3 mb-md-0 btn btn-primary btn-round" style="max-width: 250px;">
                                    <a class="d-block text-light" style="pointer-events:all;" title="Visualizza richiesta" href="{{ route('frontend.prize-status', $movement->coupon->slug) }}">VISUALIZZA RICHIESTA</a>
                                </div>
                                @endif
                                <div class="movements-item-points-amount movements-item-meta @if(!($movement->operation->id == 5)) ml-auto @endif">
                                    <i class="material-icons align-middle {{($movement->type === 'up') ? 'movement-up' : 'movement-down' }}">
                                        {{($movement->type === 'up') ? 'arrow_drop_up' : 'arrow_drop_down' }}</i>
                                    <span class="font-weight-bold">{{ (($movement->type === 'up') ? '+' : '-').$movement->amount }} punti</span>
                                </div>
                            </div>
                        </li>
                    @empty
                        <p>Nessun movimento effettuato.</p>
                    @endforelse
                </ul>
            </section>

            <section id="premi">
                <div class="heading">
                    <h3>Premi</h3>
                </div>
                <div class="premi-slider">
                    <div class="swiper-container">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            @foreach($coupons as $coupon)
                                <div class="swiper-slide">
                                    <div class="card card-product animated fadeInRight">
                                        <div class="card-header card-header-image">
                                            <a href="{{ route('frontend.show-coupon', $coupon->slug) }}">
                                                <span class="badge shadow badge-pill badge-primary">{{ $coupon->location }}</span>
                                                <img class="img" src="@if($coupon->img == 'coupon_img.png'){{ asset('img/coupons/coupon_img.png')}}@else{{ asset('storage/images/coupons/'.$coupon->img)}}@endif">
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="{{ route('frontend.show-coupon', $coupon->slug) }}">{{ $coupon->title }}</a>
                                            </h4>
                                        </div>
                                        <div class="card-footer">
                                            @if(!Auth::user()->coupons->contains($coupon))
                                                <div class="stats">
                                                    <p class="card-category"><i
                                                                class="material-icons">star</i> {{$coupon->value}} punti
                                                    </p>
                                                </div>
                                                <div class="stats">
                                                    <p class="card-category">
                                                        <i class="material-icons"> {{ $coupon->Category->icon }}</i>
                                                        {{ $coupon->Category->name }}
                                                    </p>
                                                </div>
                                            @else
                                                <div class="stats">
                                                    <p class="card-category"><i class="material-icons">done</i>
                                                        Richiesto il
                                                        <strong>{{ \Carbon\Carbon::parse(Auth::user()->CouponPivot($coupon->id)->created_at)->format('d/m/Y') }}</strong>
                                                    </p>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div class="modal fade filter" id="upload_modal" tabindex="-1" role="dialog" aria-labelledby="upload_modal"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="upload_modal">Carica Foto</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="upload-demo"></div>
                        <div class="d-flex justify-content-center">
                            <label class="btn btn-primary btn-round" for="image_file">
                                <input id="image_file" type="file" style="display:none"
                                       onchange="$('#upload-file-info').html(this.files[0].name)">
                                Scegli File
                            </label>
                        </div>
                        <div class="d-flex justify-content-center">
                            <span class='label label-info' id="upload-file-info"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">Chiudi</button>
                        <button id="upload_btn" class="btn btn-primary upload-image disabled" data-dismiss="modal">
                            Carica immagine
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@push('js')
    <script type="text/javascript">
        //IMAGE_UPLOAD
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var resize = $('#upload-demo').croppie({
            enableExif: true,
            enableOrientation: true,
            viewport: { // Default { width: 100, height: 100, type: 'square' }
                width: 200,
                height: 200,
                type: 'circle' //square
            },
            boundary: {
                width: 300,
                height: 300
            }
        });
        var extension = "";
        $('#image_file').on('change', function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                resize.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    console.log('jQuery bind complete');
                });
            };
            var re = /(?:\.([^.]+))?$/;
            filename = $(this).val().replace(/^.*[\\\/]/, '');
            extension = re.exec(filename)[1];
            console.log(extension);
            reader.readAsDataURL(this.files[0]);
            $('#upload_btn').removeClass('disabled');
        });
        $('.upload-image').on('click', function (ev) {
            ev.preventDefault();
            resize.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (img) {
                var html = '<img class="profile-img" src="' + img + '" />';
                $('.sidebar .photo img').attr('src', img);
                $("#preview-crop-image").html(html);
                $("#preview-img").html(html);
                $.ajax({
                    url: "/crop-image",
                    type: "POST",
                    data: {
                        "image": img,
                        "extension": extension
                    },
                    success: function (data) {
                        var filename = data.img;
                        $('input[name="avatar"]').val(filename);
                        $('.profile-photo').css('background-image', 'url(' + "{{ asset('storage/images/users')}}" + '/' + data.img +')');
                        let button_html = '<i class="material-icons mr-2" style="font-size:16px;">cached</i>Change Image';
                        $.notify({
                            icon: "done",
                            message: "Immagine profilo aggiornata con successo!"

                        }, {
                            type: 'success',
                            timer: 2000,
                            placement: {
                                from: 'bottom',
                                align: 'right'
                            }
                        });
                        $('#modal_toggle').html(button_html);
                    },
                    error: function (response) {
                        console.log(response);
                    }
                });
            });
        });

        $('#validate-prize').on('click', function (e) {
            var prizeName = $(e.target).attr('data-prize-name');
            var customer = $(e.target).attr('data-customer-name');
            var requestUrl = $(e.target).attr('data-url');
            var movementId = $(e.target).attr('data-movement');
            swal({
                title: 'Confermi la validazione per questo premio?',
                html: "Premio: " + prizeName + "<br> Cliente: " + customer,
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Annulla',
                confirmButtonText: 'Conferma',
                reverseButtons: true,
            }).then(function (response) {
                if (!response.dismiss) {
                    $.ajax({
                        url: requestUrl,
                        type: "PUT",
                        success: function (response) {
                            console.log(response);
                            response.message && alert(response.message);
                            $('#validation-' + movementId).html('Codice: <strong>' + response.validationCode + '</strong>');
                            swal({
                                title: 'Operazione completata!',
                                text: "Premio validato con successo!",
                                type: 'success',
                                showConfirmButton: false,
                                showCancelButton: true,
                                cancelButtonText: 'Ok'
                            })
                        },
                        error: function (response) {
                            console.log(response);
                        }
                    })
                }
            })
        })

    </script>
@endpush