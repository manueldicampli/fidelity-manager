<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="width=device-width" name="viewport" />
    <!--[if !mso]><!-->
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <!--<![endif]-->
    <title></title>
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css" />
    <!--<![endif]-->
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
        }

        table,
        td,
        tr {
            vertical-align: top;
            border-collapse: collapse;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors=true] {
            color: inherit !important;
            text-decoration: none !important;
        }
    </style>
    <style id="media-query" type="text/css">
        @media screen and (max-width:670px) {
        .block-grid,
        .col {
            min-width: 320px !important;
            max-width: 100% !important;
            display: block !important;
        }

        .block-grid {
            width: 100% !important;
        }

        .col {
            width: 100% !important;
        }

        .col_cont {
            margin: 0 auto;
        }

        img.fullwidth,
        img.fullwidthOnMobile {
            max-width: 100% !important;
        }

        .no-stack .col {
            min-width: 0 !important;
            display: table-cell !important;
        }

        .no-stack.two-up .col {
            width: 50% !important;
        }

        .no-stack .col.num2 {
            width: 16.6% !important;
        }

        .no-stack .col.num3 {
            width: 25% !important;
        }

        .no-stack .col.num4 {
            width: 33% !important;
        }

        .no-stack .col.num5 {
            width: 41.6% !important;
        }

        .no-stack .col.num6 {
            width: 50% !important;
        }

        .no-stack .col.num7 {
            width: 58.3% !important;
        }

        .no-stack .col.num8 {
            width: 66.6% !important;
        }

        .no-stack .col.num9 {
            width: 75% !important;
        }

        .no-stack .col.num10 {
            width: 83.3% !important;
        }

        .video-block {
            max-width: none !important;
        }

        .mobile_hide {
            min-height: 0px;
            max-height: 0px;
            max-width: 0px;
            display: none;
            overflow: hidden;
            font-size: 0px;
        }

        .desktop_hide {
            display: block !important;
            max-height: none !important;
        }
        }
    </style>
    <style id="icon-media-query" type="text/css">
        @media screen and (max-width:670px) {
            .icons-inner {
            text-align: center;
        }

        .icons-inner td {
            margin: 0 auto;
        }
        }
    </style>
</head>

<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #FFFFFF;">
    <!--[if IE]>
<div class="ie-browser"><![endif]-->
    <table bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" class="nl-container" role="presentation"
        style="table-layout: fixed; vertical-align: top; min-width: 320px; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFFFFF; width: 100%;"
        valign="top" width="100%">
        <tbody>
            <tr style="vertical-align: top;" valign="top">
                <td style="word-break: break-word; vertical-align: top;" valign="top">
                    <!--[if (mso)|(IE)]>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="center" style="background-color:#FFFFFF"><![endif]-->
                    <div style="background-color:transparent;">
                        <div class="block-grid no-stack"
                            style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]>
                        <td align="center" width="650"
                            style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 0px; padding-left: 10px; padding-top:10px; padding-bottom:15px;">
                        <![endif]-->
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:10px; padding-bottom:15px; padding-right: 0px; padding-left: 10px;">
                                            <!--<![endif]-->
                                            <div align="center" class="img-container center fixedwidth"
                                                style="padding-right: 0px;padding-left: 0px;">
                                                <!--[if mso]>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr style="line-height:0px">
                                                <td style="padding-right: 0px;padding-left: 0px;" align="center">
                                        <![endif]--><img align="center" alt="Alternate text" border="0"
                                                    class="center fixedwidth"
                                                    src="{{ asset('img/cozzolino_hotels_long.png') }}"
                                                    style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 288px; display: block;"
                                                    title="Alternate text" width="288" />
                                                <!--[if mso]></td></tr></table><![endif]-->
                                            </div>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    @if($movement->operation->id != 5)
                        <div style="background-color:transparent;">
                            <div class="block-grid"
                                style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                                <div
                                    style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                    <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                    <!--[if (mso)|(IE)]>
                        <td align="center" width="650"
                            style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;">
                        <![endif]-->
                                    <div class="col num12"
                                        style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div
                                                style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                <!--<![endif]-->
                                                <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 10px; padding-top: 5px; padding-bottom: 5px; font-family: Arial, sans-serif">
                                    <![endif]-->
                                                <div
                                                    style="color:#555555;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:10px;">
                                                    <div class="txtTinyMce-wrapper"
                                                        style="line-height: 1.2; font-size: 12px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #555555; mso-line-height-alt: 14px;">
                                                        <p
                                                            style="font-size: 20px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 24px; margin: 0;">
                                                            <span style="font-size: 20px; color: #339966;"><strong><span
                                                                        style="color: #d29836;">NUOVO
                                                                        MOVIMENTO</span></strong></span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <!--[if mso]></td></tr></table><![endif]-->
                                                <!--[if (!mso)&(!IE)]><!-->
                                            </div>
                                            <!--<![endif]-->
                                        </div>
                                    </div>
                                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                </div>
                            </div>
                        </div>
                        <div style="background-color:transparent;">
                            <div class="block-grid"
                                style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                                <div
                                    style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                    <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                    <!--[if (mso)|(IE)]>
                        <td align="center" width="650"
                            style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;">
                        <![endif]-->
                                    <div class="col num12"
                                        style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div
                                                style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                <!--<![endif]-->
                                                <div align="center" class="button-container"
                                                    style="padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
                                                    <!--[if mso]>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                               style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td style="padding-top: 0px; padding-right: 10px; padding-bottom: 0px; padding-left: 10px"
                                                    align="center">
                                                    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                                                 xmlns:w="urn:schemas-microsoft-com:office:word" href=""
                                                                 style="height:24pt;width:83.25pt;v-text-anchor:middle;"
                                                                 arcsize="63%"
                                                                 stroke="false" fillcolor="#d29836">
                                                        <w:anchorlock/>
                                                        <v:textbox inset="0,0,0,0">
                                                            <center style="color:#ffffff; font-family:Arial, sans-serif; font-size:12px">
                                        <![endif]-->
                                                    <div
                                                        style="text-decoration:none;display:inline-block;color:#ffffff;background-color:#d29836;border-radius:20px;-webkit-border-radius:20px;-moz-border-radius:20px;width:auto; width:auto;;border-top:0px solid #D7CCB3;border-right:0px solid #D7CCB3;border-bottom:0px solid #D7CCB3;border-left:0px solid #D7CCB3;padding-top:0px;padding-bottom:0px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;">
                                                        <span
                                                            style="padding-left:20px;padding-right:20px;font-size:12px;display:inline-block;letter-spacing:undefined;"><span
                                                                style="font-size: 16px; line-height: 2; word-break: break-word; mso-line-height-alt: 32px;"><span
                                                                    data-mce-style="font-size: 12px; line-height: 24px;"
                                                                    style="font-size: 12px; line-height: 24px;">Effettuato
                                                                    il:
                                                                    {{ \Carbon\Carbon::parse($movement->created_at)->format('d/m/Y') }}</span></span></span>
                                                    </div>
                                                    <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                                                </div>
                                                <!--[if (!mso)&(!IE)]><!-->
                                            </div>
                                            <!--<![endif]-->
                                        </div>
                                    </div>
                                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                </div>
                            </div>
                        </div>

                        <div style="background-color:transparent;">
                            <div class="block-grid"
                                style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                                <div
                                    style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                    <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                    <!--[if (mso)|(IE)]>
                        <td align="center" width="650"
                            style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 20px; padding-left: 20px; padding-top:0px; padding-bottom:0px;">
                        <![endif]-->
                                    <div class="col num12"
                                        style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div
                                                style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 20px; padding-left: 20px;">
                                                <!--<![endif]-->
                                                <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 10px; padding-top: 5px; padding-bottom: 15px; font-family: Arial, sans-serif">
                                    <![endif]-->
                                                <div
                                                    style="color:#555555;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:5px;padding-right:10px;padding-bottom:15px;padding-left:10px;">
                                                    <div class="txtTinyMce-wrapper"
                                                        style="line-height: 1.2; font-size: 12px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #555555; mso-line-height-alt: 14px;">
                                                        <p
                                                            style="font-size: 16px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 19px; margin: 0; margin-bottom:15px">
                                                            <strong style="font-size: 24px">Ciao {{ $movement->card->user->name }},</strong>
                                                        </p>
                                                        <p
                                                            style="font-size: 16px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 19px; margin: 0;">
                                                            <span style="font-size: 16px;">questo è il resoconto del
                                                                movimento sulla tua Cozzolino Hotels Fidelity card
                                                                effettuato presso <span
                                                                    style="color: #d29836;">{{ $movement->location }}</span></span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <!--[if mso]></td></tr></table><![endif]-->
                                                <!--[if (!mso)&(!IE)]><!-->
                                            </div>
                                            <!--<![endif]-->
                                        </div>
                                    </div>
                                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                </div>
                            </div>
                        </div>
                    @else
                        <div style="background-color:transparent;">
                            <div class="block-grid"
                                style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                                <div
                                    style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                    <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
                                    <div class="col num12"
                                        style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div
                                                style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                <!--<![endif]-->
                                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 5px; padding-bottom: 5px; font-family: Arial, sans-serif"><![endif]-->
                                                <div
                                                    style="color:#555555;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:10px;">
                                                    <div class="txtTinyMce-wrapper"
                                                        style="line-height: 1.2; font-size: 12px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #555555; mso-line-height-alt: 14px;">
                                                        <p
                                                            style="font-size: 20px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 24px; margin: 0;">
                                                            <span style="font-size: 20px; color: #339966;"><strong><span
                                                                        style="color: #d29836;">Complimenti!
                                                                    Hai richiesto con successo il tuo premio Fidelity!</span></strong></span></p>
                                                    </div>
                                                </div>
                                                <!--[if mso]></td></tr></table><![endif]-->
                                                <!--[if (!mso)&(!IE)]><!-->
                                            </div>
                                            <!--<![endif]-->
                                        </div>
                                    </div>
                                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                </div>
                            </div>
                        </div>
                    @endif
                    <div style="background-color:transparent;">
                        <div class="block-grid"
                            style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]>
                        <td align="center" width="650"
                            style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;">
                        <![endif]-->
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <table border="0" cellpadding="0" cellspacing="0" class="divider"
                                                role="presentation"
                                                style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                valign="top" width="100%">
                                                <tbody>
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td class="divider_inner"
                                                            style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 0px; padding-right: 20px; padding-bottom: 0px; padding-left: 20px;"
                                                            valign="top">
                                                            <table align="left" border="0" cellpadding="0"
                                                                cellspacing="0" class="divider_content"
                                                                role="presentation"
                                                                style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 3px solid #D29836; width: 100%;"
                                                                valign="top" width="100%">
                                                                <tbody>
                                                                    <tr style="vertical-align: top;" valign="top">
                                                                        <td style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                            valign="top"><span></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    @if($movement->operation->id == 5)
                        <div style="background-color:transparent;">
                            <div class="block-grid"
                                style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                                <div
                                    style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                    <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:15px; padding-bottom:0px;"><![endif]-->
                                    <div class="col num12"
                                        style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div
                                                style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:15px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                                <!--<![endif]-->
                                                <table cellpadding="0" cellspacing="0" role="presentation"
                                                    style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"
                                                    valign="top" width="100%">
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td align="center"
                                                            style="word-break: break-word; vertical-align: top; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-align: center; width: 100%;"
                                                            valign="top" width="100%">
                                                            <h1
                                                                style="color:#555555;direction:ltr;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;font-size:27px;font-weight:normal;letter-spacing:normal;line-height:120%;text-align:center;margin-top:0;margin-bottom:0;">
                                                                <strong>Ciao {{ $movement->card->user->name }},</strong></h1>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!--[if (!mso)&(!IE)]><!-->
                                            </div>
                                            <!--<![endif]-->
                                        </div>
                                    </div>
                                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                </div>
                            </div>
                        </div>

                        <div style="background-color:transparent;">
                            <div class="block-grid"
                                style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                                <div
                                    style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                    <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
                                    <div class="col num12"
                                        style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div
                                                style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                <!--<![endif]-->
                                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top: 20px; padding-bottom: 20px; font-family: Arial, sans-serif"><![endif]-->
                                                <div
                                                    style="color:#393d47;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;">
                                                    <div class="txtTinyMce-wrapper"
                                                        style="line-height: 1.2; font-size: 12px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #393d47; mso-line-height-alt: 14px;">
                                                        <p
                                                            style="font-size: 16px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 19px; margin: 0;">
                                                            <span style="font-size: 16px;">ti ringraziamo per aver
                                                                scelto di
                                                                far parte del programma <strong>Cozzolino Hotels
                                                                    Fidelity</strong>.</span></p>
                                                        <p
                                                            style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                                             </p>
                                                        <p
                                                            style="font-size: 18px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 22px; margin: 0;">
                                                            <span style="font-size: 18px;"><strong><span style="">In
                                                                        merito
                                                                        alla tua richiesta ti lasciamo qui di seguito le
                                                                        modalità per <span
                                                                            style="color: #d29836;">contattare la
                                                                            struttura
                                                                            e concordare il ritiro / prenotazione del
                                                                            tuo
                                                                            premio. </span></span></strong></span></p>
                                                        <p
                                                            style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                                             </p>
                                                        <p
                                                            style="font-size: 16px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 19px; margin: 0;">
                                                            <span style="font-size: 16px;">Ricorda che in seguito alla validazione del tuo
                                                                coupon premio avrai bisogno di mostrare il codice presente nella pagina di stato della richiesta che trovi cliccando sul pulsante <strong><span
                                                                        style="color: #d29836;">"VISUALIZZA
                                                                        RICHIESTA"</span>
                                                                </strong>qui in basso.</span></p>
                                                        <p
                                                            style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                                             </p>
                                                        <p
                                                            style="font-size: 16px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 19px; margin: 0;">
                                                            <span style="font-size: 16px;"><strong>Il presente coupon
                                                                    sarà
                                                                    valido solo previa attivazione da parte di Cozzolino
                                                                    Hotels.</strong></span></p>
                                                    </div>
                                                </div>
                                                <!--[if mso]></td></tr></table><![endif]-->
                                                <!--[if (!mso)&(!IE)]><!-->
                                            </div>
                                            <!--<![endif]-->
                                        </div>
                                    </div>
                                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                </div>
                            </div>
                        </div>
                        <div style="background-color:transparent;">
                            <div class="block-grid two-up"
                                style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                                <div
                                    style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                    <!--[if (mso)|(IE)]><td align="center" width="325" style="background-color:transparent;width:325px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top:0px; padding-bottom:0px;"><![endif]-->
                                    <div class="col num6"
                                        style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 324px; width: 325px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div
                                                style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:0px; padding-right: 20px; padding-left: 20px;">
                                                <!--<![endif]-->
                                                <div align="center" class="img-container center autowidth"
                                                    style="padding-right: 0px;padding-left: 0px;">
                                                    <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><![endif]--><img
                                                        align="center" alt="I'm an image" border="0"
                                                        class="center autowidth" src="{{ asset('img/coupons/'.$prize->img) }}"
                                                        style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; border-radius: 10px; -webkit-border-radius: 10px; -moz-border-radius: 10px; width: 100%; max-width: 285px; display: block;"
                                                        title="Immagine premio" width="285" />
                                                    <!--[if mso]></td></tr></table><![endif]-->
                                                </div>
                                                <!--[if (!mso)&(!IE)]><!-->
                                            </div>
                                            <!--<![endif]-->
                                        </div>
                                    </div>
                                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                    <!--[if (mso)|(IE)]></td><td align="center" width="325" style="background-color:transparent;width:325px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top:15px; padding-bottom:5px;"><![endif]-->
                                    <div class="col num6"
                                        style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 324px; width: 325px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div
                                                style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:15px; padding-bottom:5px; padding-right: 20px; padding-left: 20px;">
                                                <!--<![endif]-->
                                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
                                                <div
                                                    style="color:#555555;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div class="txtTinyMce-wrapper"
                                                        style="line-height: 1.2; font-size: 12px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #555555; mso-line-height-alt: 14px;">
                                                        <p
                                                            style="font-size: 22px; line-height: 1.2; text-align: left; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 26px; margin: 0;">
                                                            <span
                                                                style="font-size: 22px;"><strong>{{ $prize->title }}</strong></span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <!--[if mso]></td></tr></table><![endif]-->
                                                <table border="0" cellpadding="0" cellspacing="0" class="divider"
                                                    role="presentation"
                                                    style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    valign="top" width="100%">
                                                    <tbody>
                                                        <tr style="vertical-align: top;" valign="top">
                                                            <td class="divider_inner"
                                                                style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;"
                                                                valign="top">
                                                                <table align="center" border="0" cellpadding="0"
                                                                    cellspacing="0" class="divider_content"
                                                                    role="presentation"
                                                                    style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 1px solid #BBBBBB; width: 100%;"
                                                                    valign="top" width="100%">
                                                                    <tbody>
                                                                        <tr style="vertical-align: top;" valign="top">
                                                                            <td style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                                valign="top"><span></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
                                                <div
                                                    style="color:#555555;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div class="txtTinyMce-wrapper"
                                                        style="line-height: 1.2; font-size: 12px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #555555; mso-line-height-alt: 14px;">
                                                        <p
                                                            style="font-size: 14px; line-height: 1.2; text-align: left; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                                            {!! $prize->description !!}</p>
                                                    </div>
                                                </div>
                                                <!--[if mso]></td></tr></table><![endif]-->
                                                <div align="left" class="button-container"
                                                    style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td style="padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px" align="left"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://fidelity.manueldicampli.it" style="height:31.5pt;width:189.75pt;v-text-anchor:middle;" arcsize="58%" stroke="false" fillcolor="#d29836"><w:anchorlock/><v:textbox inset="0,0,0,0"><center style="color:#ffffff; font-family:Arial, sans-serif; font-size:16px"><![endif]--><a
                                                        href="{{route('frontend.prize-status', $prize->slug)}}"
                                                        style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #d29836; border-radius: 24px; -webkit-border-radius: 24px; -moz-border-radius: 24px; width: auto; width: auto; border-top: 1px solid #d29836; border-right: 1px solid #d29836; border-bottom: 1px solid #d29836; border-left: 1px solid #d29836; padding-top: 5px; padding-bottom: 5px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"
                                                        target="_blank"><span
                                                            style="padding-left:20px;padding-right:20px;font-size:16px;display:inline-block;letter-spacing:undefined;"><span
                                                                style="font-size: 16px; margin: 0; line-height: 2; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 32px;">VISUALIZZA
                                                                RICHIESTA</span></span></a>
                                                    <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                                                </div>
                                                <!--[if (!mso)&(!IE)]><!-->
                                            </div>
                                            <!--<![endif]-->
                                        </div>
                                    </div>
                                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                </div>
                            </div>
                        </div>
                    @endif
                    <div style="background-color:transparent;">
                        <div class="block-grid mixed-two-up no-stack"
                            style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]>
                        <td align="center" width="108"
                            style="background-color:transparent;width:108px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;">
                        <![endif]-->
                                <div class="col num2"
                                    style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 108px; width: 108px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <table cellpadding="0" cellspacing="0" role="presentation"
                                                style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"
                                                valign="top" width="100%">
                                                <tr style="vertical-align: top;" valign="top">
                                                    <td style="word-break: break-word; vertical-align: top; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;"
                                                        valign="top">
                                                        <table align="center" cellpadding="0" cellspacing="0"
                                                            role="presentation"
                                                            style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"
                                                            valign="top">
                                                            <tr style="vertical-align: top;" valign="top">
                                                                <td align="center"
                                                                    style="word-break: break-word; vertical-align: top; text-align: center; padding-top: 25px; padding-bottom: 25px; padding-left: 25px; padding-right: 25px;"
                                                                    valign="top"><img align="center" alt="" class="icon"
                                                                        height="32"
                                                                        src="{{ asset('img/icons/location_pin.png') }}"
                                                                        style="border:0;" width="null" /></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td>
                        <td align="center" width="541"
                            style="background-color:transparent;width:541px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 5px; padding-left: 0px; padding-top:20px; padding-bottom:5px;">
                        <![endif]-->
                                <div class="col num10"
                                    style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 540px; width: 541px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:5px; padding-right: 5px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 15px; padding-top: 0px; padding-bottom: 0px; font-family: Arial, sans-serif">
                                    <![endif]-->
                                            <div
                                                style="color:#555555;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:15px;">
                                                <div class="txtTinyMce-wrapper"
                                                    style="line-height: 1.2; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; color: #555555; mso-line-height-alt: 14px;">
                                                    <p
                                                        style="font-size: 18px; line-height: 1.2; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 22px; margin: 0;">
                                                        <span style="font-size: 18px;"><strong>Location:
                                                                {{ $movement->location }}<br /></strong></span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                            <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 15px; padding-left: 15px; padding-top: 10px; padding-bottom: 5px; font-family: Arial, sans-serif">
                                    <![endif]-->
                                            <div
                                                style="color:#555555;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:15px;padding-bottom:5px;padding-left:15px;">
                                                <div class="txtTinyMce-wrapper"
                                                    style="line-height: 1.2; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; color: #555555; mso-line-height-alt: 14px;">
                                                    @if($movement->location == 'Hotel Promenade')
                                                        <p
                                                            style="font-size: 16px; line-height: 1.2; word-break: break-word; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 19px; margin: 0;">
                                                            <span style="font-size: 16px;">Via Aldo Moro, 63 -
                                                                Montesilvano Spiaggia (PE) 65013</span>
                                                        </p>
                                                    @else
                                                        <p
                                                            style="font-size: 16px; line-height: 1.2; word-break: break-word; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 19px; margin: 0;">
                                                            <span style="font-size: 16px;">C.da Santa Calcagna, 71/72 -
                                                                Rocca San Giovanni (CH) 66020</span>
                                                        </p>
                                                    @endif
                                                </div>
                                            </div>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    @if($movement->operation->id == 5)
                    <div style="background-color:transparent;">
                        <div class="block-grid mixed-two-up no-stack"
                            style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]><td align="center" width="108" style="background-color:transparent;width:108px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;"><![endif]-->
                                <div class="col num2"
                                    style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 108px; width: 108px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <table cellpadding="0" cellspacing="0" role="presentation"
                                                style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"
                                                valign="top" width="100%">
                                                <tbody>
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td style="word-break: break-word; vertical-align: top; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;"
                                                            valign="top">
                                                            <table align="center" cellpadding="0" cellspacing="0"
                                                                role="presentation"
                                                                style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"
                                                                valign="top">
                                                                <tbody>
                                                                    <tr style="vertical-align: top;" valign="top">
                                                                        <td align="center"
                                                                            style="word-break: break-word; vertical-align: top; text-align: center; padding-top: 25px; padding-bottom: 25px; padding-left: 25px; padding-right: 25px;"
                                                                            valign="top"><img align="center" alt=""
                                                                                class="icon" height="32"
                                                                                src="{{ asset('img/icons/email.png') }}" style="border:0;"
                                                                                width="null"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td><td align="center" width="541" style="background-color:transparent;width:541px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 5px; padding-left: 0px; padding-top:20px; padding-bottom:5px;"><![endif]-->
                                <div class="col num10"
                                    style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 540px; width: 541px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:5px; padding-right: 5px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 15px; padding-left: 15px; padding-top: 10px; padding-bottom: 10px; font-family: Georgia, 'Times New Roman', serif"><![endif]-->
                                            <div
                                                style="color:#555555;font-family:'Droid Serif', Georgia, Times, 'Times New Roman', serif;line-height:1.2;padding-top:10px;padding-right:15px;padding-bottom:10px;padding-left:15px;">
                                                <div class="txtTinyMce-wrapper"
                                                     style="line-height: 1.2; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; color: #555555; mso-line-height-alt: 14px;">
                                                <p style="font-size: 16px; line-height: 1.2; word-break: break-word; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 19px; margin: 0;">
                                                        <span style="font-size: 18px;">
                                                        <strong>Email:@if($movement->location == 'Hotel Promenade') info@hotelpromenadeabruzzo.it @else info@hotelvillamediciabruzzo.it @endif</strong></span></p>
                                                </div>
                                            </div>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid mixed-two-up no-stack"
                            style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]><td align="center" width="108" style="background-color:transparent;width:108px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;"><![endif]-->
                                <div class="col num2"
                                    style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 108px; width: 108px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <table cellpadding="0" cellspacing="0" role="presentation"
                                                style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"
                                                valign="top" width="100%">
                                                <tbody>
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td style="word-break: break-word; vertical-align: top; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;"
                                                            valign="top">
                                                            <table align="center" cellpadding="0" cellspacing="0"
                                                                role="presentation"
                                                                style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"
                                                                valign="top">
                                                                <tbody>
                                                                    <tr style="vertical-align: top;" valign="top">
                                                                        <td align="center"
                                                                            style="word-break: break-word; vertical-align: top; text-align: center; padding-top: 25px; padding-bottom: 25px; padding-left: 25px; padding-right: 25px;"
                                                                            valign="top"><img align="center" alt=""
                                                                                class="icon" height="32"
                                                                                src="{{ asset('img/icons/phone-call.png') }}"
                                                                                style="border:0;" width="null"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td><td align="center" width="541" style="background-color:transparent;width:541px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 5px; padding-left: 0px; padding-top:20px; padding-bottom:5px;"><![endif]-->
                                <div class="col num10"
                                    style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 540px; width: 541px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:5px; padding-right: 5px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 15px; padding-left: 15px; padding-top: 10px; padding-bottom: 10px; font-family: Georgia, 'Times New Roman', serif"><![endif]-->
                                            <div
                                                style="color:#555555;font-family:'Droid Serif', Georgia, Times, 'Times New Roman', serif;line-height:1.2;padding-top:10px;padding-right:15px;padding-bottom:10px;padding-left:15px;">
                                                <div class="txtTinyMce-wrapper"
                                                     style="line-height: 1.2; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; color: #555555; mso-line-height-alt: 14px;">
                                                    <p style="font-size: 16px; line-height: 1.2; word-break: break-word; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 19px; margin: 0;">
                                                        <span style="font-size: 18px;"><strong>Tel: @if($movement->location == 'Hotel Promenade') 085 445 2221 @else 0872 717645 @endif</strong></span></p>
                                                </div>
                                            </div>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    @endif
                    <div style="background-color:transparent;">
                        <div class="block-grid mixed-two-up no-stack"
                            style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]>
                        <td align="center" width="108"
                            style="background-color:transparent;width:108px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;">
                        <![endif]-->
                                <div class="col num2"
                                    style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 108px; width: 108px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <table cellpadding="0" cellspacing="0" role="presentation"
                                                style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"
                                                valign="top" width="100%">
                                                <tr style="vertical-align: top;" valign="top">
                                                    <td style="word-break: break-word; vertical-align: top; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;"
                                                        valign="top">
                                                        <table align="center" cellpadding="0" cellspacing="0"
                                                            role="presentation"
                                                            style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"
                                                            valign="top">
                                                            <tr style="vertical-align: top;" valign="top">
                                                                <td align="center"
                                                                    style="word-break: break-word; vertical-align: top; text-align: center; padding-top: 25px; padding-bottom: 25px; padding-left: 25px; padding-right: 25px;"
                                                                    valign="top"><img align="center" alt="" class="icon"
                                                                        height="32" src="{{ $movement_icon_src }}"
                                                                        style="border:0;" width="null" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td>
                        <td align="center" width="541"
                            style="background-color:transparent;width:541px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 5px; padding-left: 0px; padding-top:20px; padding-bottom:5px;">
                        <![endif]-->
                                <div class="col num10"
                                    style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 540px; width: 541px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:5px; padding-right: 5px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 15px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif">
                                    <![endif]-->
                                            <div
                                                style="color:#555555;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:15px;">
                                                <div class="txtTinyMce-wrapper"
                                                    style="line-height: 1.2; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; color: #555555; mso-line-height-alt: 14px;">
                                                    <p
                                                        style="font-size: 18px; line-height: 1.2; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 22px; margin: 0;">
                                                        <span style="font-size: 18px;"><strong>Tipologia:
                                                                {{ $movement->operation->name }}</strong></span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid mixed-two-up no-stack"
                            style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]>
                        <td align="center" width="108"
                            style="background-color:transparent;width:108px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;">
                        <![endif]-->
                                <div class="col num2"
                                    style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 108px; width: 108px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <table cellpadding="0" cellspacing="0" role="presentation"
                                                style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"
                                                valign="top" width="100%">
                                                <tr style="vertical-align: top;" valign="top">
                                                    <td style="word-break: break-word; vertical-align: top; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;"
                                                        valign="top">
                                                        <table align="center" cellpadding="0" cellspacing="0"
                                                            role="presentation"
                                                            style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;"
                                                            valign="top">
                                                            <tr style="vertical-align: top;" valign="top">
                                                                <td align="center"
                                                                    style="word-break: break-word; vertical-align: top; text-align: center; padding-top: 25px; padding-bottom: 25px; padding-left: 25px; padding-right: 25px;"
                                                                    valign="top"><img align="center" alt="" class="icon"
                                                                        height="32"
                                                                        src="{{ asset('img/icons/credit_card.png') }}"
                                                                        style="border:0;" width="null" /></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td>
                        <td align="center" width="541"
                            style="background-color:transparent;width:541px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 5px; padding-left: 0px; padding-top:20px; padding-bottom:5px;">
                        <![endif]-->
                                <div class="col num10"
                                    style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 540px; width: 541px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:5px; padding-right: 5px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 15px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif">
                                    <![endif]-->
                                            <div
                                                style="color:#555555;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:15px;">
                                                <div class="txtTinyMce-wrapper"
                                                    style="line-height: 1.2; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 12px; color: #555555; mso-line-height-alt: 14px;">
                                                    <p
                                                        style="font-size: 18px; line-height: 1.2; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 22px; margin: 0;">
                                                        <span style="font-size: 18px;"><strong>Card
                                                                N°: {{ $movement->card->number }}</strong></span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid"
                            style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]>
                        <td align="center" width="650"
                            style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:10px;">
                        <![endif]-->
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:10px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <table border="0" cellpadding="0" cellspacing="0" class="divider"
                                                role="presentation"
                                                style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                valign="top" width="100%">
                                                <tbody>
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td class="divider_inner"
                                                            style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 0px; padding-right: 20px; padding-bottom: 5px; padding-left: 20px;"
                                                            valign="top">
                                                            <table align="left" border="0" cellpadding="0"
                                                                cellspacing="0" class="divider_content"
                                                                role="presentation"
                                                                style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 3px solid #D29836; width: 100%;"
                                                                valign="top" width="100%">
                                                                <tbody>
                                                                    <tr style="vertical-align: top;" valign="top">
                                                                        <td style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                            valign="top"><span></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid two-up"
                            style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: #d29836;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#d29836;">
                                <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:#d29836"><![endif]-->
                                <!--[if (mso)|(IE)]>
                        <td align="center" width="325"
                            style="background-color:#d29836;width:325px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 0px; padding-left: 0px; padding-top:20px; padding-bottom:20px;">
                        <![endif]-->
                                <div class="col num6"
                                    style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 324px; width: 325px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:20px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <div align="center" class="button-container"
                                                style="padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;">
                                                <!--[if mso]>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                               style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px"
                                                    align="center">
                                                    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                                                 xmlns:w="urn:schemas-microsoft-com:office:word" href=""
                                                                 style="height:24.75pt;width:228.75pt;v-text-anchor:middle;"
                                                                 arcsize="13%"
                                                                 stroke="false" fill="false">
                                                        <w:anchorlock/>
                                                        <v:textbox inset="0,0,0,0">
                                                            <center style="color:#ffffff; font-family:Arial, sans-serif; font-size:16px">
                                        <![endif]-->
                                                <div
                                                    style="text-decoration:none;display:inline-block;color:#ffffff;background-color:transparent;border-radius:4px;-webkit-border-radius:4px;-moz-border-radius:4px;width:auto; width:auto;;border-top:0px dashed #2A9A5F;border-right:0px dashed #2A9A5F;border-bottom:0px dashed #2A9A5F;border-left:0px dashed #2A9A5F;padding-top:5px;padding-bottom:0px;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;text-align:center;mso-border-alt:none;word-break:keep-all;">
                                                    <span
                                                        style="padding-left:20px;padding-right:20px;font-size:16px;display:inline-block;letter-spacing:undefined;">
                                                        <span
                                                            style="font-size: 16px; line-height: 1.2; word-break: break-word; mso-line-height-alt: 19px;">
                                                            <span data-mce-style="font-size: 24px; line-height: 28px;"
                                                                style="font-size: 24px; line-height: 28px;">
                                                                @if($movement->type == 'up')
                                                                    <span
                                                                        data-mce-style="line-height: 19px; font-size: 16px;"
                                                                        style="line-height: 19px; font-size: 16px;">Hai
                                                                        guadagnato</span>
                                                                @else
                                                                    <span
                                                                        data-mce-style="line-height: 19px; font-size: 16px;"
                                                                        style="line-height: 19px; font-size: 16px;">Hai
                                                                        utilizzato</span>
                                                                @endif
                                                                <span data-mce-style="line-height: 14px;"
                                                                    style="line-height: 14px;"><strong>{{ $movement->amount }}
                                                                        punti</strong></span>
                                                            </span></span></span>
                                                </div>
                                                <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                                            </div>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td>
                        <td align="center" width="325"
                            style="background-color:#d29836;width:325px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 15px; padding-left: 0px; padding-top:5px; padding-bottom:5px;">
                        <![endif]-->
                                <div class="col num6"
                                    style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 324px; width: 325px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 15px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <div align="center" class="button-container"
                                                style="padding-top:10px;padding-right:0px;padding-bottom:10px;padding-left:0px;">
                                                <!--[if mso]>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                               style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td style="padding-top: 10px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px"
                                                    align="center">
                                                    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                                                 xmlns:w="urn:schemas-microsoft-com:office:word"
                                                                 href="https://fidelity.manueldicampli.it/"
                                                                 style="height:31.5pt;width:228pt;v-text-anchor:middle;"
                                                                 arcsize="58%"
                                                                 stroke="false" fillcolor="#ffffff">
                                                        <w:anchorlock/>
                                                        <v:textbox inset="0,0,0,0">
                                                            <center style="color:#d29836; font-family:Arial, sans-serif; font-size:16px">
                                        <![endif]-->
                                                <a href="{{ route('home') }}"
                                                    style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #d29836; background-color: #ffffff; border-radius: 24px; -webkit-border-radius: 24px; -moz-border-radius: 24px; width: auto; width: auto; border-top: 0px solid TRANSPARENT; border-right: 0px solid TRANSPARENT; border-bottom: 0px solid TRANSPARENT; border-left: 0px solid TRANSPARENT; padding-top: 5px; padding-bottom: 5px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;"
                                                    target="_blank"><span
                                                        style="padding-left:15px;padding-right:15px;font-size:16px;display:inline-block;letter-spacing:undefined;"><span
                                                            style="font-size: 16px; line-height: 2; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 32px;"><span
                                                                data-mce-style="font-size: 16px; line-height: 32px;"
                                                                style="font-size: 16px; line-height: 32px;">VAI ALLA TUA
                                                                AREA RISERVATA</span></span></span></a>
                                                <!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
                                            </div>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid"
                            style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]>
                        <td align="center" width="650"
                            style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 0px; padding-left: 0px; padding-top:15px; padding-bottom:0px;">
                        <![endif]-->
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:15px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 10px; padding-top: 15px; padding-bottom: 15px; font-family: Arial, sans-serif">
                                    <![endif]-->
                                            <div
                                                style="color:#d29836;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.5;padding-top:15px;padding-right:10px;padding-bottom:15px;padding-left:10px;">
                                                <div class="txtTinyMce-wrapper"
                                                    style="line-height: 1.5; font-size: 12px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #d29836; mso-line-height-alt: 18px;">
                                                    <p
                                                        style="text-align: center; line-height: 1.5; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 18px; margin: 0;">
                                                        <strong><span style="font-size: 18px;">SALDO
                                                                DISPONIBILE</span></strong>
                                                    </p>
                                                    <p
                                                        style="text-align: center; line-height: 1.5; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; mso-line-height-alt: 24px; margin: 0;">
                                                        <span style="font-size: 16px; color: #999999;">(aggiornato al
                                                            {{ \Carbon\Carbon::now()->format('d/m/Y') }}):</span>
                                                    </p>
                                                    <p
                                                        style="text-align: center; line-height: 1.5; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 18px; margin: 0;">
                                                         </p>
                                                    <p
                                                        style="text-align: center; line-height: 1.5; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 34px; mso-line-height-alt: 51px; margin: 0;">
                                                        <span style="font-size: 34px;"><strong>{{ $user_points }}
                                                                punti</strong></span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid"
                             style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                    style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;"><![endif]-->
                                <div class="col num12"
                                     style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                                style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <table border="0" cellpadding="0" cellspacing="0" class="divider"
                                                   role="presentation"
                                                   style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                   valign="top" width="100%">
                                                <tbody>
                                                <tr style="vertical-align: top;" valign="top">
                                                    <td class="divider_inner"
                                                        style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;"
                                                        valign="top">
                                                        <table align="right" border="0" cellpadding="0"
                                                               cellspacing="0" class="divider_content"
                                                               role="presentation"
                                                               style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 3px solid #D29836; width: 100%;"
                                                               valign="top" width="100%">
                                                            <tbody>
                                                            <tr style="vertical-align: top;" valign="top">
                                                                <td style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                    valign="top"><span></span></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    @if($user_points < 400 && $user_points >= 350)
                    <div style="background-color:transparent;">
                        <div class="block-grid"
                            style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]>
                        <td align="center" width="650"
                            style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 0px; padding-left: 0px; padding-top:15px; padding-bottom:0px;">
                        <![endif]-->
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:15px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 10px; padding-top: 15px; padding-bottom: 15px; font-family: Arial, sans-serif">
                                    <![endif]-->
                                            <div
                                                style="color:#d29836;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.5;padding-top:15px;padding-right:10px;padding-bottom:15px;padding-left:10px;">
                                                <div class="txtTinyMce-wrapper"
                                                    style="line-height: 1.5; font-size: 12px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #d29836; mso-line-height-alt: 18px;">
                                                    <p
                                                        style="text-align: center; line-height: 1.5; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 18px; margin: 0;">
                                                        <strong><span style="font-size: 18px;"><strong>COMPLIMENTI!</strong><br> Ti {{ (400 - $user_points) == 1 ? 'restano' : 'resta' }} soltanto {{ (400 - $user_points) == 1 ? 'un punto' : 'altri' }} <strong>{{ 350 - $user_points }} {{ (400 - $user_points) != 1 ? 'punti' : '' }}</strong> per richiedere il tuo primo premio fidelity!</span></strong>
                                                    </p>
                                                    
                                                </div>
                                            </div>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    @endif

                    @if($movement->operation->id == 5)
                        <div style="background-color:transparent;">
                            <div class="block-grid"
                                style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                                <div
                                    style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                    <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                    <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;"><![endif]-->
                                    <div class="col num12"
                                        style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div
                                                style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                                <!--<![endif]-->
                                                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 15px; padding-bottom: 15px; font-family: Arial, sans-serif"><![endif]-->
                                                <div
                                                    style="color:#d29836;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.5;padding-top:15px;padding-right:10px;padding-bottom:15px;padding-left:10px;">
                                                    <div class="txtTinyMce-wrapper"
                                                        style="line-height: 1.5; font-size: 12px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #d29836; mso-line-height-alt: 18px;">
                                                        <p
                                                            style="text-align: center; line-height: 1.5; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; mso-line-height-alt: 24px; margin: 0;">
                                                            <span style="font-size: 16px; color: #999999;">Per maggiori
                                                                informazioni:</span></p>
                                                        <p
                                                            style="text-align: center; line-height: 1.5; word-break: break-word; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 16px; mso-line-height-alt: 24px; margin: 0;">
                                                            <span style="font-size: 16px;"><a
                                                                    href="mailto:cozzolinohotels@gmail.com?subject=Richiesta%20informazioni"
                                                                    rel="noopener"
                                                                    style="text-decoration: none; color: #d29836;"
                                                                    target="_blank"
                                                                    title="cozzolinohotels@gmail.com">cozzolinohotels@gmail.com</a></span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <!--[if mso]></td></tr></table><![endif]-->
                                                <table border="0" cellpadding="0" cellspacing="0" class="divider"
                                                    role="presentation"
                                                    style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    valign="top" width="100%">
                                                    <tbody>
                                                        <tr style="vertical-align: top;" valign="top">
                                                            <td class="divider_inner"
                                                                style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;"
                                                                valign="top">
                                                                <table align="left" border="0" cellpadding="0"
                                                                    cellspacing="0" class="divider_content"
                                                                    role="presentation"
                                                                    style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 3px solid #D29836; width: 100%;"
                                                                    valign="top" width="100%">
                                                                    <tbody>
                                                                        <tr style="vertical-align: top;" valign="top">
                                                                            <td style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                                valign="top"><span></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!--[if (!mso)&(!IE)]><!-->
                                            </div>
                                            <!--<![endif]-->
                                        </div>
                                    </div>
                                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                </div>
                            </div>
                        </div>
                    @endif
                    <div style="background-color:transparent;">
                        <div class="block-grid"
                            style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                               style="background-color:transparent;">
                            <tr>
                                <td align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                        <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]>
                        <td align="center" width="650"
                            style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="padding-right: 0px; padding-left: 0px; padding-top:25px; padding-bottom:25px;">
                        <![endif]-->
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:25px; padding-bottom:25px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 10px; padding-top: 0px; padding-bottom: 0px; font-family: Arial, sans-serif">
                                    <![endif]-->
                                            <div
                                                style="color:#555555;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
                                                <div class="txtTinyMce-wrapper"
                                                    style="line-height: 1.2; font-size: 12px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #555555; mso-line-height-alt: 14px;">
                                                    <p
                                                        style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                                        <span style="color: #ffffff;"><span style="color: #d29836;">©
                                                                Cozzolino Hotels
                                                                {{ \Carbon\Carbon::now()->format('Y') }}</span><br /></span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>

                    <div style="background-color:transparent;">
                        <div class="block-grid"
                             style="min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                            <div
                                    style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <!--[if (mso)|(IE)]>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                       style="background-color:transparent;">
                                    <tr>
                                        <td align="center">
                                            <table cellpadding="0" cellspacing="0" border="0" style="width:650px">
                                                <tr class="layout-full-width" style="background-color:transparent"><![endif]-->
                                <!--[if (mso)|(IE)]>
                                <td align="center" width="650"
                                    style="background-color:transparent;width:650px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                                    valign="top">
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 0px; padding-left: 0px; padding-top:25px; padding-bottom:25px;">
                                <![endif]-->
                                <div class="col num12"
                                     style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div class="col_cont" style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                                style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:25px; padding-bottom:25px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <!--[if mso]>
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="padding-right: 10px; padding-left: 10px; padding-top: 0px; padding-bottom: 0px; font-family: Arial, sans-serif">
                                            <![endif]-->
                                            <div
                                                    style="color:#555555;font-family:'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
                                                <div class="txtTinyMce-wrapper"
                                                     style="line-height: 1.2; font-size: 12px; font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif; color: #555555; mso-line-height-alt: 14px;">
                                                    <p
                                                            style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Nunito, Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 17px; margin: 0;">
                                                        <span style="color: #ffffff;"><span style="color: #d29836;">Ricevi questa mail perchè sei iscritto al programma fidelity Cozzolino Hotels.</span><br /></span>
                                                    </p>
                                                </div>
                                            </div>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
    <!--[if (IE)]></div><![endif]-->
</body>

</html>