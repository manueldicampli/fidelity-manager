@extends('layouts.app', ['activePage' => 'coupon', 'title' => __('Premi'), 'class' => 'cards_mobile loading', 'target' => '#coupon_content', 'model_search' => 'coupon.search'])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col">
            <div class="card-container">
              <div class="card-header card-header-primary">
                <div class="row">
                    <div class="col-9 col-md-10 text-left">
                        <h4 class="card-title ">{{ __('Lista Premi') }}</h4>
                        <p class="card-category"> {{ __('Qui puoi gestire tutti i premi') }}</p>
                    </div>
                    <div class="col-3 col-md-2 text-right d-flex align-items-center justify-content-end">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                              <a class="nav-link btn btn-white btn-round btn-fab px-2" href="#" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons" style="font-size:20px;">more_vert</i>
                                <div class="ripple-container"></div>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right" id="optionsMenu" aria-labelledby="navbarDropdownProfile">
                                <a class="dropdown-item" href="{{ route('coupon.create') }}">{{ __('Aggiungi premio') }}</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#filterDialog"><i class="material-icons mr-2" style="font-size:16px;">filter_list</i>Filtra</a>
                              </div>
                            </li>
                          </ul>
                    </div>
                  </div>
              </div>
              <div id="coupon_content">
                @include('coupon.content')
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
<div class="modal fade filter" id="filterDialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Filtra Premi</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form autocomplete="off" data-ajax="#coupon_content" action="{{ route('coupon.search')}}" method="get">
          @csrf
          @method('get')
        <div class="modal-body">
            <h4 class="mt-3 mb-0">Per Hotel</h4>
            <hr class="mt-2">
            <ul class="nav nav-pills nav-pills-warning nav-pills-icons p-0 hotels">
                <li class="nav-item">
                    <label class="nav-link" for="villa-medici">
                        <img src="{{asset('img/icons/villa-medici.png')}}"> Hotel Villa Medici
                    </label>
                    <input type="checkbox" name="location[]" id="villa-medici" value="Hotel Villa Medici" hidden>
                </li>
                <li class="nav-item">
                    <label class="nav-link" for="promenade">
                        <img src="{{asset('img/icons/promenade.png')}}"> Hotel Promenade
                    </label>
                    <input type="checkbox" name="location[]" id="promenade" value="Hotel Promenade" hidden>
                </li>
            </ul>
            <h4 class="mt-3 mb-0">Seleziona Categoria/e</h4>
            <hr class="mt-2">
            {{--<ul class="nav nav-pills nav-pills-warning nav-pills-icons p-0">
              @foreach ($categories as $category)

                <li class="nav-item">
                  <label class="nav-link" for="{{ $category->name }}">
                    <i class="material-icons">{{ $category->icon }}</i> {{ $category->name }}
                  </label>
                  <input type="checkbox" name="category[]" id="{{ $category->name }}" value="{{ $category->id }}" hidden>
                </li> 
              @endforeach
            </ul>--}}
            @foreach ($categories as $category)
            <div class="form-check">
                <label class="form-check-label" for="category-{{ $category->id }}">
                    <input class="form-check-input" type="checkbox" name="category[]" id="category-{{ $category->id }}" value="{{ $category->id }}">
                    {{ $category->name }}
                    <span class="form-check-sign">
                        <span class="check"></span>
                    </span>
                </label>
            </div>
            @endforeach
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Go</button>
        </div>
      </form>
      </div>
    </div>
  </div>

@endsection

@if(session('status'))
  @push('js')
      <script type="text/javascript">
        $.notify({
            icon: "done",
            message: "{{ session('status') }}"

        },{
            type: 'success',
            timer: 2000,
            placement: {
                from: 'bottom',
                align: 'right'
            }
        });
      </script>
  @endpush
@endif