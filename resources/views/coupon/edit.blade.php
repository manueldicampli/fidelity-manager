@extends('layouts.app', ['activePage' => 'coupon-management', 'title' => __('Modifica Coupon'), 'class' => 'cards_mobile'])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('coupon.update', $coupon) }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

            <div class="card ">
              <div class="card-header card-header-primary">
                <div class="row">
                  <div class="col-9 col-md-10 text-left d-flex align-items-center">
                      <h4 class="card-title ">{{ __('Modifica Coupon') }}</h4>
                  </div>
                  <div class="col-3 col-md-2 text-right d-flex align-items-center justify-content-end">
                    <a href="{{ route('coupon.index') }}" style="display: none" class="btn btn-sm btn-white d-md-block">{{ __('TORNA ALLA LISTA') }}</a>
                    <a href="{{ route('coupon.index') }}" class="btn btn-fab btn-white btn-round d-md-none"><i class="material-icons">arrow_back</i></a>
                  </div>
                </div>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('Titolo') }}</label>
                        <div class="col-sm-7">
                          <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" id="input-title" type="text" placeholder="{{ __('Inserisci il nome del premio') }}" value="{{ old('title', $coupon->title) }}" required="true" aria-required="true"/>
                            @if ($errors->has('title'))
                              <span id="title-error" class="error text-danger" for="input-title">{{ $errors->first('title') }}</span>
                            @endif
                          </div>
                        </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-2 col-form-label">{{ __('Scadenza') }}</label>
                      <div class="col-sm-7">
                        <div class="form-group{{ $errors->has('deadline') ? ' has-danger' : '' }}">
                          <input class="form-control datetimepicker{{ $errors->has('deadline') ? ' is-invalid' : '' }}" name="deadline" id="input-deadline" placeholder="{{ __('Seleziona una data di scadenza...') }}" value="{{ old('deadline', \Carbon\Carbon::parse($coupon->deadline)->format('d/m/Y')) }}" required>
                          @if ($errors->has('deadline'))
                            <span id="deadline-error" class="error text-danger" for="input-deadline">{{ $errors->first('deadline') }}</span>
                          @endif
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{ __('Categoria') }}</label>
                        <div class="col-sm-7">
                          <div class="form-group">
                              <select class="selectpicker" name="category_id" id="input-category" data-style="btn btn-primary btn-round" data-size="7">
                                <option value="0">Per favore seleziona una categoria...</option>
                                @foreach ($categories as $category)
                                  <option value="{{ $category->id }}" @isset($coupon->category)@if($coupon->category->id == $category->id)  selected @endif @endisset >{{ $category->name }}</option>
                                @endforeach
                              </select>
                              @if ($errors->has('category_id'))
                              <span id="category-error" class="error text-danger float-left" for="input-category">Please select a valid category.</span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="row">
                          <label class="col-sm-2 col-form-label">{{ __('Hotel') }}</label>
                          <div class="col-sm-7">
                              <div class="form-group{{ $errors->has('location') ? ' has-danger' : '' }}">
                                  <select name="location" class="selectpicker"
                                          data-style="btn btn-primary btn-round" data-size="7">
                                      <option value="Hotel Promenade" @isset($coupon->location)@if($coupon->location == 'Hotel Promenade')  selected @endif @endisset>Hotel Promenade</option>
                                      <option value="Hotel Villa Medici" @isset($coupon->location)@if($coupon->location == 'Hotel Villa Medici')  selected @endif @endisset>Hotel Villa Medici</option>
                                  </select>
                                  @if ($errors->has('location'))
                                      <span id="company-error" class="error text-danger" for="input-company">{{ $errors->first('location') }}</span>
                                  @endif
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <label class="col-sm-2 col-form-label">{{ __('Valore in punti') }}</label>
                          <div class="col-sm-7">
                              <div class="form-group{{ $errors->has('value') ? ' has-danger' : '' }}">
                                  <input class="form-control{{ $errors->has('value') ? ' is-invalid' : '' }}" name="value" id="input-value" placeholder="{{ __('Insersici qui il valore del premio in punti') }}" value="{{ old('value', $coupon->value) }}" required>
                                  @if ($errors->has('value'))
                                      <span id="deadline-error" class="error text-danger" for="input-deadline">{{ $errors->first('value') }}</span>
                                  @endif
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <label class="col-sm-2 col-form-label">{{ __('Disponibilità') }}</label>
                          <div class="col-sm-7 d-flex align-items-end">
                              <div class="togglebutton">
                                  <label class="mb-0">
                                      <input type="checkbox" class="bootstrap-switch" data-toggle="switch" data-on-label="Disponibile" data-off-label="Non disponibile" {{$coupon->availability == 'available' ? 'checked' : ''}}>
                                      <span class="toggle"></span>
                                      <input type="hidden" name="availability" value="{{ $coupon->availability ? old('value', $coupon->availability): ''}}">
                                      <span id="label">{{$coupon->availability == 'available' ? 'Disponibile' : 'Non disponibile'}}</span>
                                  </label>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Immagine</label>
                        <div class="col-sm-7 mb-3">
                          <button type="button" class="btn btn-primary btn-round mb-3" data-toggle="modal" data-target="#upload_modal" id="modal_toggle"><i class="material-icons mr-2" style="font-size:16px;">cached</i>Modifica immagine</button>
                          <div id="preview-img">
                            <img src="@if($coupon->img == 'coupon_img.png'){{ asset('img/coupons/coupon_img.png')}}@else{{ asset('storage/images/coupons/'.$coupon->img)}}@endif">
                          </div>
                        </div>
                        <input name="img" value="{{ $coupon->img }}" hidden>
                      </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-1 col-form-label">{{ __('Descrizione') }}</label>
                  <div class="col-sm-11 mb-3">
                    <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                      <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="editor" placeholder="{{ __('Inserisci qui la descrizione del premio') }}">{{ old('description', $coupon->description) }}</textarea>
                      @if ($errors->has('description'))
                        <span id="description-error" class="error text-danger" for="input-description">{{ $errors->first('description') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade filter" id="upload_modal" tabindex="-1" role="dialog" aria-labelledby="upload_modal" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="upload_modal">Carica immagine</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div id="upload-demo"></div>
              <div class="d-flex justify-content-center">
                <label class="btn btn-primary btn-round" for="image_file">
                    <input id="image_file" type="file" style="display:none" val="{{$coupon->img}}"
                    onchange="$('#upload-file-info').html(this.files[0].name)">
                    Seleziona file
                </label>
              </div>
              <div class="d-flex justify-content-center">
                <span class='label label-info' id="upload-file-info"></span>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">Chiudi</button>
            <button id="upload_btn" class="btn btn-primary upload-image disabled" data-dismiss="modal">Conferma upload</button>
          </div>
        </div>
      </div>
    </div>

@endsection
@push('js')
  <script type="text/javascript">

      $(document).ready( function() {
          $('.bootstrap-switch').each(function() {
              $this = $(this);
              data_on_label = $this.data('on-label');
              data_off_label = $this.data('off-label');

              if($this.prop('checked')) {
                  $this.parents('label').find('#label').html(data_on_label);
              } else {
                  $this.parents('label').find('#label').html(data_off_label);
              }
          });
          $('.bootstrap-switch').click(function(){
              $this = $(this);
              data_on_label = $this.data('on-label');
              data_off_label = $this.data('off-label');

              if($this.prop('checked')) {
                  $this.parents('label').find('input[type="hidden"]').val('available');
                $this.parents('label').find('#label').html(data_on_label);
              } else {
                  $this.parents('label').find('input[type="hidden"]').val('not_available');
                  $this.parents('label').find('#label').html(data_off_label);
              }
          });
      });

    //DATETIMEPICKER init
    $('.datetimepicker').datetimepicker({
      icons: {
          time: "fa fa-clock-o",
          date: "fa fa-calendar",
          up: "fa fa-chevron-up",
          down: "fa fa-chevron-down",
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-screenshot',
          clear: 'fa fa-trash',
          close: 'fa fa-remove'
      },
      format:'DD/MM/YYYY'
    });

    //CKEDITOR init
    ClassicEditor
          .create( document.querySelector( '#editor' ) )
          .catch( error => {
              console.error( error );
          } );

    //CROPPIE
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    var resize = $('#upload-demo').croppie({
        enableExif: true,
        enableOrientation: true,    
        viewport: { // Default { width: 100, height: 100, type: 'square' } 
            width: 200,
            height: 133,
            //type: 'circle' //square
        },
        boundary: {
            width: 300,
            height: 233
        }
    });
    $('#image_file').on('change', function () { 
      var reader = new FileReader();
        reader.onload = function (e) {
          resize.croppie('bind',{
            url: e.target.result
          }).then(function(){
            console.log('jQuery bind complete');
          });
        }
        reader.readAsDataURL(this.files[0]);
        $('#upload_btn').removeClass('disabled');
    });

    $('.upload-image').on('click', function (ev) {
        ev.preventDefault();
      resize.croppie('result', {
        type: 'canvas',
        size: {width:820}
      }).then(function (img) {
        html = '<img src="' + img + '" />';
        $("#preview-crop-image").html(html);
        $("#preview-img").html(html);
        $("input[name='img']").val(img);
        let button_html = '<i class="material-icons mr-2" style="font-size:16px;">cached</i>Change Image'
        $('#modal_toggle').html(button_html);
      });
    });

    // $('.upload-image').on('click', function (ev) {
    //     ev.preventDefault();
    //   resize.croppie('result', {
    //     type: 'canvas',
    //     size: {width:820}
    //   }).then(function (img) {
    //     html = '<img src="' + img + '" />';
    //     $("#preview-crop-image").html(html);
    //     $("#preview-img").html(html);
    //     $.ajax({
    //       url: "{{route('upload-image')}}",
    //       type: "POST",
    //       data: {
    //         "image":img
    //         },
    //       success: function (data) {
    //         console.log(data);
    //         var filename = data.img;
    //         $('input[name="img"]').attr('value', filename);
    //         let button_html = '<i class="material-icons mr-2" style="font-size:16px;">cached</i>Change Image'
    //         $('#modal_toggle').html(button_html);
    //       },
    //       error: function(response){
    //         console.log(response);
    //       }
    //     });
    //   });
    // });
  </script>
@endpush