<div class="card-body">
    <div class="row">
      @foreach ($coupons as $coupon)
        <div class="col-md-6 col-xl-4">
        <div class="card card-product animated fadeInRight" style="animation-delay:.{{$loop->index}}s;" data-count="3">
          <div class="card-header card-header-image" data-header-animation="true">
            <a href="{{ route('coupon.show', $coupon)}}">
                <div class="d-flex justify-content-between badges">
                    <span class="badge shadow badge-pill badge-light">{{ $coupon->location }}</span>
                    <span class="badge shadow badge-pill badge-light"><span class="{{ $coupon->availability }}"></span>{{ $coupon->availability == 'available' ? 'Disponibile' : 'Non disponibile' }}</span>
                </div>
              <img class="img" src="@if($coupon->img == 'coupon_img.png'){{ asset('img/coupons/coupon_img.png')}}@else{{ asset('storage/images/coupons/'.$coupon->img)}}@endif">
            </a>
          </div>
          <div class="card-body">
            <div class="card-actions text-center">
              <form action="{{ route('coupon.destroy', $coupon) }}" method="post">
                  @csrf
                  @method('delete')
                <a class="btn btn-default btn-link" href="{{ route('coupon.show', $coupon)}}">
                  <i class="material-icons">art_track</i>
                </a>
                <a class="btn btn-success btn-link" href="{{ route('coupon.edit', $coupon) }}">
                  <i class="material-icons">edit</i>
                </a>
                <button type="button" class="btn btn-danger btn-link" onclick="confirm('{{ __("Sei veramente sicuro di eliminare questo premio?") }}') ? this.parentElement.submit() : ''">
                    <i class="material-icons">close</i>
                </button>
              </form>
            </div>
            <h4 class="card-title">
              <a href="#pablo">{{ $coupon->title }}</a>
            </h4>
            <div class="card-description">
                {!! $coupon->description !!}
            </div>
          </div>
          <div class="card-footer">
              <div class="stats">
                  <p class="card-category"><i class="material-icons">star</i> {{ $coupon->value }} punti</p>
              </div>
            <div class="stats">
              <p class="card-category">
                <i class="material-icons">{{$coupon->category->icon ?? 'category'}}</i>
                  {{ $coupon->category->name ?? 'Uncategorized' }}
            </div>
          </div>
        </div>
      </div>
      @endforeach
      @empty($coupon)
          <div class="d-flex justify-content-center my-3 w-100">
            <img class="no_coupons" src="{{ asset('img')}}/no_coupons.jpg">
          </div>
          <h4 class="d-flex justify-content-center mb-0 text-muted w-100">Nessun coupon</h4>
      @endempty
    </div>
    <div class="ajax_paginate" data-ajax="#coupon_content">
      {{ $coupons->links() }}
    </div>
    </div>
  </div>