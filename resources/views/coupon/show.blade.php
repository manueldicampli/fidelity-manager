@extends('layouts.app', ['activePage' => 'coupon', 'class' => 'show', 'title' => $coupon->title.' - '.$category_name])
@section('content')
@guest
  @include('layouts.navbars.navs.guest')
@endguest
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 coupon-img d-md-flex order-md-12" style="display:none">
          <img class="img" src="@if($coupon->img == 'coupon_img.png'){{ asset('img/coupons/coupon_img.png')}}@else{{ asset('storage/images/coupons/'.$coupon->img)}}@endif">
      </div>
      <div class="col-md-6 order-md-1">
        <div class="card-container">
          <div class="card-header card-header-primary">
            <div>
                <div class="text-left">
                  <h4 class="card-title mb-2">{{ $coupon->title }}</h4>
                </div>
                <span class="label label-inv mr-2"><i class="material-icons">star</i>{{ $coupon->value }} punti</span>
                <span class="label label-inv mr-2"><i class="material-icons">{{ $coupon->category->icon ?? 'category' }}</i>{{ $category_name }}</span>
                <span class="label label-inv">{{ $coupon->location }}</span>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6 coupon-img d-md-none">
                <img class="img my-1" src="@if($coupon->img == 'coupon_img.png'){{ asset('img/coupons/coupon_img.png')}}@else{{ asset('storage/images/coupons/'.$coupon->img)}}@endif">
              </div>
              <div class="col info-coupon">
                <div class="description">
                  <h3><i class="material-icons mr-2">subject</i>Descrizione premio</h3>
                  {!! $coupon->description !!}
                </div>
                <div class="deadline mb-4">
                    <h3><i class="material-icons mr-2">date_range</i>Data Scadenza</h3>
                    <h4>{{ \Carbon\Carbon::parse($coupon->deadline)->format('d/m/Y') }}</h4>
                </div>
                <button class="btn btn-primary btn-link btn-lg clipboard" data-toggle="tooltip" data-trigger="click" title="Copiato negli appunti!" style="color: white!important; border-radius:999px;" data-clipboard-text="{{route('guest.show-coupon', $coupon->slug)}}"><span class="material-icons mr-2">inventory</span>Copia link negli appunti</button>
              </div>
            </div>
            @guest
            <hr>
            {{--<div class="info-coupon">
              <h3><i class="material-icons mr-2">check</i>Registra il tuo coupon</h3>
              <form class="form" method="POST" action="{{ route('register') }}" autocomplete="off">
                @csrf
                    <div class="bmd-form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                              <i class="material-icons">face</i>
                          </span>
                        </div>
                        <input type="text" name="name" class="form-control" placeholder="{{ __('Name...') }}" value="{{ old('name') }}" required>
                      </div>
                      @if ($errors->has('name'))
                        <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                          <strong>{{ $errors->first('name') }}</strong>
                        </div>
                      @endif
                    </div>
                    <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} mt-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">email</i>
                          </span>
                        </div>
                        <input type="email" name="email" class="form-control" placeholder="{{ __('Email...') }}" value="{{ old('email') }}" required>
                      </div>
                      @if ($errors->has('email'))
                        <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                          <strong>{{ $errors->first('email') }}</strong>
                        </div>
                      @endif
                    </div>
                    <div class="bmd-form-group{{ $errors->has('password') ? ' has-danger' : '' }} mt-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">lock_outline</i>
                          </span>
                        </div>
                        <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password...') }}" required>
                      </div>
                      @if ($errors->has('password'))
                        <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                          <strong>{{ $errors->first('password') }}</strong>
                        </div>
                      @endif
                    </div>
                    <div class="bmd-form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }} mt-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="material-icons">lock_outline</i>
                          </span>
                        </div>
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ __('Confirm Password...') }}" required>
                      </div>
                      @if ($errors->has('password_confirmation'))
                        <div id="password_confirmation-error" class="error text-danger pl-3" for="password_confirmation" style="display: block;">
                          <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </div>
                      @endif
                    </div>
                    <div class="form-check mr-auto ml-3 mt-3">
                      <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" id="policy" name="policy" {{ old('policy', 1) ? 'checked' : '' }} >
                        <span class="form-check-sign">
                          <span class="check"></span>
                        </span>
                        {{ __('I agree with the ') }} <a href="#">{{ __('Privacy Policy') }}</a>
                      </label>
                    </div>
                  <input hidden name="coupon_id" value="{{$coupon->id}}">
                  <div class="card-footer justify-content-center">
                    <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Register coupon') }}</button>
                  </div>
              </form>
            </div>--}}
          @endguest
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@guest
  @include('layouts.footers.guest')
@endguest

@endsection