@extends('layouts.app', ['activePage' => 'coupon', 'class' => 'show', 'title' => $coupon->title.' - '.$category_name])
@section('content')
@guest
  @include('layouts.navbars.navs.guest')
@endguest
<div class="content">
  <div class="container-fluid">
    <div class="d-flex flex-column mx-auto" style="max-width:768px">
      <div class="coupon-img d-md-flex" style="display:none">
          <img class="img border-0" style="border-radius: 12px;" src="@if($coupon->img == 'coupon_img.png'){{ asset('img/coupons/coupon_img.png')}}@else{{ asset('storage/images/coupons/'.$coupon->img)}}@endif">
      </div>
      <div class="card-container" style="border-radius: 12px;">
        <div class="card-header card-header-primary" style="border-radius: 12px;">
          <div>
              <div class="text-left">
                <h4 class="card-title mb-2 font-weight-bold" style="font-size:25px">{{ $coupon->title }}</h4>
              </div>
              <span class="label label-inv mr-2"><i class="material-icons">{{ $coupon->category->icon ?? 'category' }}</i>{{ $category_name }}</span>
              <span class="label label-inv">{{ $coupon->location }}</span>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6 coupon-img d-md-none">
              <img class="img my-1" src="@if($coupon->img == 'coupon_img.png'){{ asset('img/coupons/coupon_img.png')}}@else{{ asset('storage/images/coupons/'.$coupon->img)}}@endif">
            </div>
            <div class="col info-coupon">
              <div class="description">
                <h3><i class="material-icons mr-2">subject</i>Descrizione premio</h3>
                {!! $coupon->description !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@guest
  @include('layouts.footers.guest')
@endguest

@endsection