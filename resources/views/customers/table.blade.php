<div class="table-responsive">
    <table class="table">
        <thead class=" text-primary">
        <th>
            {{ __('Nome') }}
        </th>
        <th>
            {{ __('Cognome') }}
        </th>
        <th>
            {{ __('Ultimo movimento') }}
        </th>
        <th class="text-right">
            {{ __('Azioni') }}
        </th>
        </thead>
        <tbody>
        @forelse($users as $user)
            <tr>
                <td>
                    {{ $user->name }}
                </td>
                <td>
                    {{ $user->surname }}
                </td>
                <td>
                    @if($user->CustomerMovements()->exists())
                    {{ \Carbon\Carbon::parse($user->CustomerMovements->last()['created_at'])->format('d/m/Y').' - '}}@if($user->CustomerMovements->last()->partial)(PAR) @endif{{$user->CustomerMovements->last()->operation->name}}
                        @if($user->CustomerMovements->last()->operation->id == 5) : {{\Illuminate\Support\Str::limit($user->CustomerMovements->last()->coupon->title, 15, $end='...') }}@endif
                        <i class="material-icons align-middle {{($user->CustomerMovements->last()['type'] === 'up') ? 'movement-up' : 'movement-down' }}">{{($user->CustomerMovements->last()['type'] === 'up') ? 'arrow_drop_up' : 'arrow_drop_down' }}</i>
                        <strong>{{ (($user->CustomerMovements->last()['type'] === 'up') ? '+' : '-').$user->CustomerMovements->last()['amount'] }} punti</strong>
                    @else
                        <p class="m-0"><i class="material-icons align-bottom">notification_important</i> Nessun movimento per questo cliente</p>
                    @endif
                </td>
                <td class="td-actions text-right">
                    @if ($user->id != auth()->id())
                        <button class="btn btn-primary btn-link" style="background: none !important; box-shadow: none!important;" href="#" data-toggle="modal" data-target="#modelId" data-ajax="#modal_content" data-method="get" data-action="{{ route('customers.show', $user)}}">
                            <i class="material-icons">remove_red_eye</i>
                        </button>
                        <button class="btn btn-primary btn-link" style="background: none !important; box-shadow: none!important;" href="#" data-toggle="modal" data-target="#modelId" data-ajax="#modal_content" data-method="get" data-action="{{ route('customers.cards', $user)}}">
                            <i class="material-icons">credit_card</i>
                        </button>
                        <a class="btn btn-success btn-link" href="{{ route('customers.edit', $user) }}">
                            <i class="material-icons">edit</i>
                        </a>
                        <form action="{{ route('customers.destroy', $user) }}" method="post" class="d-inline-block">
                            @csrf
                            @method('delete')
                            <button type="button" class="btn btn-danger btn-link" onclick="confirm('{{ __("Sei sicuro di voler eliminare questo cliente?") }}') ? this.parentElement.submit() : ''">
                                <i class="material-icons">close</i>
                            </button>
                        </form>
                    @else
                        <a class="btn btn-success btn-link" href="{{ route('profile.edit') }}" >
                            <i class="material-icons">edit</i>
                        </a>
                    @endif
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4">
                    <p id="no-customers" class="text-center font-weight-bold">
                        <i class="material-icons align-bottom">notification_important</i> Nessun cliente trovato
                    </p>
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>