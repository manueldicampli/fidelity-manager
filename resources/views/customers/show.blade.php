<div class="modal-header">
    <div class="user mb-3">
        <div class="photo mr-3 mb-3 mb-md-0">
            <img src="{{ $customer->avatar != '' ? asset('storage/images/users/'.$customer->avatar) : asset('img/users/user.jpg')}}">
        </div>
        <div class="user-info py-1">
            <h4>{{ $customer->name.' '.$customer->surname }}</h4>
            <small><strong>{{ $customer->company }}</strong>@if($customer->company) - @endif{{ $customer->job }}</small>
        </div>
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body pt-0">
    <div class="row">
        <div class="col-12 info-box">
            <h4>Email</h4>
            <a href="mailto:{{ $customer->email }}">{{ $customer->email }}</a>
        </div>
        <div class="col-md-6 info-box">
            <h4>Recapito telefonico</h4>
            <p>{{$customer->phone}}</p>
        </div>
        <div class="col-md-6 info-box">
            <h4>Sesso</h4>
            <p>{{$customer->gender}}</p>
        </div>
        <div class="col-md-6 info-box">
            <h4>Provincia</h4>
            <p>{{$provincia}}</p>
        </div>
        <div class="col-md-6 info-box">
            <h4>Data di nascita</h4>
            <p>{{$customer->birthday}}</p>
        </div>
        <div class="col-md-6 info-box">
            <h4>Agenzia</h4>
            <p>{{$customer->company}}</p>
        </div>
        <div class="col-md-6 info-box">
            <h4>Professione</h4>
            <p>{{$customer->job}}</p>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary close-btn" data-dismiss="modal">Chiudi</button>
</div>