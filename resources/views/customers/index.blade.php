@extends('layouts.app', ['activePage' => 'customers-management', 'title' => __('Gestione Clienti')])

<style>
    .swal2-popup .swal2-styled {
        border-radius:999px!important;
    }
    .swal2-popup .swal2-styled.swal2-confirm {
        background: #d29836 !important;
        box-shadow: 0 4px 20px 0 rgb(0 0 0 / 14%), 0 7px 10px -5px #d2983663 !important;
    }
</style>

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="row justify-content-between align-items-center">
                                <div class="col-sm-6">
                                    <div class="title text-center text-sm-left mb-2 mt-0 mb-sm-0">
                                        <h4 class="card-title ">{{ __('Lista Clienti') }}</h4>
                                        <p class="card-category"> {{ __('Qui puoi gestire i clienti') }}</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="customer-search" style="width: 220px;">
                                        <input class="form-control" style="width: 185px;" type="text" placeholder="Nome / Cognome / N.Card" name="search" data-ajax="#customers_table" data-action="{{route('customers.search')}}" data-method="get" value="@isset($search){{ $search != '' ? $search : '' }}@endisset">
                                        <i class="material-icons">search</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="alert alert-success">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <i class="material-icons">close</i>
                                            </button>
                                            <span>{{ session('status') }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{ route('customers.create') }}"
                                       class="btn btn-sm btn-primary">{{ __('Aggiungi cliente') }}</a>
                                </div>
                            </div>
                            <div id="customers_table">
                                @include('customers.table')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div id="modal_content">
                    @isset($customer)
                        @include('customers.modal')
                    @endisset
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        function validatePrize(e) {
            console.log('clicked');
            var prizeName = $(e.target).attr('data-prize-name');
            var customer = $(e.target).attr('data-customer-name');
            var requestUrl = $(e.target).attr('data-url');
            var prizeId = $(e.target).attr('data-prize');
            var customerID = $(e.target).attr('data-customer');
            swal({
                title: 'Confermi la validazione per questo premio?',
                html: "Premio: " + prizeName + "<br> Cliente: " + customer,
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Annulla',
                confirmButtonText: 'Conferma',
                reverseButtons: true,
            }).then(function (response) {
                if (!response.dismiss) {
                    $.ajax({
                        url: requestUrl,
                        type: "PUT",
                        success: function (response) {
                            console.log(response);
                            response.message && alert(response.message);
                            $('#validation-' + prizeId + '-' + customerID).html('Codice: <strong>' + response.validationCode + '</strong>');
                            swal({
                                title: 'Operazione completata!',
                                text: "Premio validato con successo!",
                                type: 'success',
                                showConfirmButton: false,
                                showCancelButton: true,
                                cancelButtonText: 'Ok'
                            })
                        },
                        error: function (response) {
                            console.log(response);
                        }
                    })
                }
            })
        }
    </script>
@endpush