@extends('layouts.app', ['activePage' => 'customers-management', 'title' => __('Aggiungi Cliente')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('customers.store') }}" autocomplete="off"
                          class="form-horizontal" validate>
                        @csrf
                        @method('post')

                        <div class="card mb-0">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Aggiungi Cliente') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('customers.index') }}"
                                           class="btn btn-sm btn-primary">{{ __('Torna alla lista') }}</a>
                                    </div>
                                </div>
                                <div class="row mx-auto mb-4" style="max-width:900px">
                                    <div class="col-md-6">
                                        <label>{{ __('Nome') }}</label>
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   name="name" id="input-name" type="text"
                                                   placeholder="{{ __('Inserisci nome') }}" value="{{ old('name') }}"
                                                   />
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger"
                                                      for="input-name">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('Cognome') }}</label>
                                        <div class="form-group{{ $errors->has('surname') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}"
                                                   name="surname" id="input-surname" type="text"
                                                   placeholder="{{ __('Inserisci cognome') }}"
                                                   value="{{ old('surname') }}"
                                                   />
                                            @if ($errors->has('surname'))
                                                <span id="surname-error" class="error text-danger"
                                                      for="input-surname">{{ $errors->first('surname') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="mb-0">{{ __('Provincia di residenza') }}</label>
                                                <div class="form-group">
                                                    <select class="selectpicker" name="city" id="input-city"
                                                            data-style="btn btn-primary btn-round" data-size="7">
                                                        <option value="0">Seleziona una provincia...</option>
                                                        <option value="foreign">Strainera</option>
                                                        @foreach ($province as $provincia)
                                                            <option value="{{ $provincia->id }}"
                                                                    @if(old('city') == $provincia->id)  selected @endif> {{ $provincia->nome }} </option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('city'))
                                                        <span id="city-error" class="error text-danger float-left"
                                                              for="input-city">Per favore seleziona una provincia.</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="mr-3 d-block" style="
                                                       margin-bottom: 25px;">{{ __('Sesso') }}</label>
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="gender"
                                                               value="M" checked=""> M
                                                        <span class="circle">
                                                  <span class="check"></span>
                                                </span>
                                                    </label>
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="gender"
                                                               value="F" checked=""> F
                                                        <span class="circle">
                                                  <span class="check"></span>
                                                </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('Telefono') }}</label>
                                        <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                                   name="phone" id="input-phone" type="text"
                                                   placeholder="{{ __('Inserisci numero di telefono') }}"
                                                   value="{{ old('phone') }}"/>
                                            @if ($errors->has('phone'))
                                                <span id="phone-error" class="error text-danger"
                                                      for="input-phone">{{ $errors->first('phone') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('Email') }}</label>
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                   name="email" id="input-email" type="email"
                                                   placeholder="{{ __('Inserisci email') }}"
                                                   value="{{ old('email') }}"/>
                                            @if ($errors->has('email'))
                                                <span id="email-error" class="error text-danger"
                                                      for="input-email">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('Azienda') }}</label>
                                        <div class="form-group{{ $errors->has('company') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}"
                                                   name="company" id="input-company" type="text"
                                                   placeholder="{{ __('Inserisci il nome dell\'azienda') }}"
                                                   value="{{ old('company') }}"
                                                   />
                                            @if ($errors->has('company'))
                                                <span id="company-error" class="error text-danger"
                                                      for="input-company">{{ $errors->first('company') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('Professione') }}</label>
                                        <div class="form-group{{ $errors->has('job') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('job') ? ' is-invalid' : '' }}"
                                                   name="job" id="input-job" type="text"
                                                   placeholder="{{ __('Inserisci la professione') }}"
                                                   value="{{ old('job') }}" />
                                            @if ($errors->has('job'))
                                                <span id="job-error" class="error text-danger"
                                                      for="input-job">{{ $errors->first('job') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('Data di nascita') }}</label>
                                        <div class="form-group{{ $errors->has('birthday') ? ' has-danger' : '' }}">
                                            <input class="form-control {{ $errors->has('birthday') ? ' is-invalid' : '' }}"
                                                   name="birthday" id="input-birthday"
                                                   placeholder="{{ __('GG/MM/AAAA') }}"
                                                   value="{{ old('Data di nascita') }}" />
                                            @if ($errors->has('birthday'))
                                                <span id="birthday-error" class="error text-danger"
                                                      for="input-birthday">{{ $errors->first('birthday') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>{{ __('Username') }}</label>
                                        <div class="form-group{{ $errors->has('username') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}"
                                                   name="username" id="input-username" type="text"
                                                   placeholder="{{ __('Inserisci uno username') }}"
                                                   value="{{ old('username') }}"/>
                                            @if ($errors->has('username'))
                                                <span id="username-error" class="error text-danger"
                                                      for="input-username">{{ $errors->first('username') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="input-password">{{ __(' Password') }}</label>
                                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                   input type="password" name="password" id="input-password"
                                                   placeholder="{{ __('Inserisci una password') }}" value="" />
                                            @if ($errors->has('password'))
                                                <span id="name-error" class="error text-danger"
                                                      for="input-name">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="input-password-confirmation">{{ __('Conferma Password') }}</label>
                                        <div class="form-group">
                                            <input class="form-control" name="password_confirmation"
                                                   id="input-password-confirmation" type="password"
                                                   placeholder="{{ __('Conferma la password') }}" value="" />
                                        </div>
                                    </div>
                                </div>
                                <hr class="mx-auto mb-4" style="max-width:900px">
                                <div class="row mx-auto mb-2" style="max-width:900px">
                                    <div class="col-12">
                                        <h4 class="card-title float-none font-weight-normal">Dati nuova card</h4>
                                        <p class="card-category mt-1 float-none">
                                            Inserisci numero e PIN della tessera
                                        </p>
                                    </div>
                                    <div class="col-md-4">
                                        <label>{{ __('Numero card') }}</label>
                                        <div class="form-group{{ $errors->has('card') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('card') ? ' is-invalid' : '' }}"
                                                   name="card" id="input-card" type="text"
                                                   placeholder="{{ __('Inserisci il numero della card') }}"
                                                   value="{{ old('card') }}"
                                                   />
                                            @if ($errors->has('card'))
                                                <span id="card-error" class="error text-danger"
                                                      for="input-card">{{ $errors->first('card') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>{{ __('Punti') }}</label>
                                        <div class="form-group{{ $errors->has('points') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('points') ? ' is-invalid' : '' }}"
                                                   name="points" id="input-points" type="number"
                                                   placeholder="{{ __('Inserisci il numero di punti') }}"
                                                   value="{{ old('points', 0) }}"
                                                   />
                                            @if ($errors->has('company'))
                                                <span id="company-error" class="error text-danger"
                                                      for="input-company">{{ $errors->first('company') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="d-block" style="
                                                       margin-bottom: 25px;">{{ __('Newsletter e sms') }}</label>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="newsletter"
                                                       value="SI" checked="true"> SI
                                                <span class="circle">
                                                  <span class="check"></span>
                                                </span>
                                            </label>
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="newsletter"
                                                       value="NO" checked=""> NO
                                                <span class="circle">
                                                  <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto mb-4">
                                <button type="submit" class="btn btn-primary">{{ __('Aggiungi Cliente') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
