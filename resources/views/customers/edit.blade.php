@extends('layouts.app', ['activePage' => 'customers-management', 'title' => __('Modifica Cliente')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form id="customer_edit" method="post" action="{{ route('customers.update', $customer) }}" autocomplete="off"
                          class="form-horizontal" validate>
                        @csrf
                        @method('put')

                        <div class="card mb-0">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Modifica Cliente') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('customers.index') }}"
                                           class="btn btn-sm btn-primary">{{ __('Torna alla lista') }}</a>
                                    </div>
                                </div>
                                <div class="row mx-auto mb-4" style="max-width:900px">
                                    <div class="col-md-6">
                                        <label>{{ __('Nome') }}</label>
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   name="name" id="input-name" type="text"
                                                   placeholder="{{ __('Inserisci nome') }}"
                                                   value="{{ $customer->name }}"
                                                   required="true" aria-required="true"/>
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger"
                                                      for="input-name">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('Cognome') }}</label>
                                        <div class="form-group{{ $errors->has('surname') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}"
                                                   name="surname" id="input-surname" type="text"
                                                   placeholder="{{ __('Inserisci cognome') }}"
                                                   value="{{ $customer->surname }}"
                                                   required="true" aria-required="true"/>
                                            @if ($errors->has('surname'))
                                                <span id="surname-error" class="error text-danger"
                                                      for="input-surname">{{ $errors->first('surname') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="mb-0">{{ __('Provincia di residenza') }}</label>
                                                <div class="form-group">
                                                    <select class="selectpicker" name="city" id="input-city"
                                                            data-style="btn btn-primary btn-round" data-size="7">
                                                        <option value="0">Seleziona una provincia...</option>
                                                        <option value="foreign">Straniera</option>
                                                        @foreach ($province as $provincia)
                                                            <option value="{{ $provincia->id }}"
                                                                    @if($customer->city == $provincia->id)  selected @endif> {{ $provincia->nome }} </option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('city'))
                                                        <span id="city-error" class="error text-danger float-left"
                                                              for="input-city">Per favore seleziona una provincia.</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="mr-3 d-block" style="
                                                       margin-bottom: 25px;">{{ __('Sesso') }}</label>
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="gender"
                                                               value="M" @if($customer->gender === 'M') checked @endif>
                                                        M
                                                        <span class="circle">
                                                  <span class="check"></span>
                                                </span>
                                                    </label>
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="gender"
                                                               value="F" @if($customer->gender === 'F') checked @endif>
                                                        F
                                                        <span class="circle">
                                                  <span class="check"></span>
                                                </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('Telefono') }}</label>
                                        <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                                   name="phone" id="input-phone" type="text"
                                                   placeholder="{{ __('Inserisci numero di telefono') }}"
                                                   value="{{ $customer->phone }}"/>
                                            @if ($errors->has('phone'))
                                                <span id="phone-error" class="error text-danger"
                                                      for="input-phone">{{ $errors->first('phone') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('Email') }}</label>
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                   name="email" id="input-email" type="email"
                                                   placeholder="{{ __('Inserisci email') }}"
                                                   value="{{ $customer->email }}"/>
                                            @if ($errors->has('email'))
                                                <span id="email-error" class="error text-danger"
                                                      for="input-email">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('Azienda') }}</label>
                                        <div class="form-group{{ $errors->has('company') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}"
                                                   name="company" id="input-company" type="text"
                                                   placeholder="{{ __('Inserisci il nome dell\'azienda') }}"
                                                   value="{{ $customer->company }}"/>
                                            @if ($errors->has('company'))
                                                <span id="company-error" class="error text-danger"
                                                      for="input-company">{{ $errors->first('company') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('Professione') }}</label>
                                        <div class="form-group{{ $errors->has('job') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('job') ? ' is-invalid' : '' }}"
                                                   name="job" id="input-job" type="text"
                                                   placeholder="{{ __('Inserisci la professione') }}"
                                                   value="{{ $customer->job }}"/>
                                            @if ($errors->has('job'))
                                                <span id="job-error" class="error text-danger"
                                                      for="input-job">{{ $errors->first('job') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('Data di nascita') }}</label>
                                        <div class="form-group{{ $errors->has('birthday') ? ' has-danger' : '' }}">
                                            <input class="form-control {{ $errors->has('birthday') ? ' is-invalid' : '' }}"
                                                   name="birthday" id="input-birthday"
                                                   placeholder="{{ __('GG/MM/AAAA') }}"
                                                   value="{{ $customer->birthday }}" required/>
                                            @if ($errors->has('birthday'))
                                                <span id="birthday-error" class="error text-danger"
                                                      for="input-birthday">{{ $errors->first('birthday') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto mb-4">
                                <button type="submit" class="btn btn-primary">{{ __('Salva Modifiche') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xl-6 mx-auto">
                        <form method="post" action="{{ route('customers.password', $customer) }}" class="form-horizontal">
                            @csrf
                            @method('put')

                            <div class="card ">
                                <div class="card-header card-header-primary">
                                    <h4 class="card-title">{{ __('Modifica password') }}</h4>
                                    <p class="card-category">{{ __('Qui puooi modificare la password del cliente') }}</p>
                                </div>
                                <div class="card-body ">
                                    @if (session('status_password'))
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <i class="material-icons">close</i>
                                                    </button>
                                                    <span>{{ session('status_password') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <label class="col-sm-4 col-form-label"
                                               for="input-password">{{ __('Nuova Password') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                                <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                       name="password" id="input-password" type="password" value="" required/>
                                                @if ($errors->has('password'))
                                                    <span id="password-error" class="error text-danger"
                                                          for="input-password">{{ $errors->first('password') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label class="col-sm-4 col-form-label"
                                               for="input-password-confirmation">{{ __('Conferma Password') }}</label>
                                        <div class="col-sm-7">
                                            <div class="form-group">
                                                <input class="form-control" name="password_confirmation"
                                                       id="input-password-confirmation" type="password" value="" required/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary">{{ __('Modifica password') }}</button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection
