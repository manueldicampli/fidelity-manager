<div class="modal-header">
    <div class="user mb-3">
        <div class="photo mr-3 mb-3 mb-md-0">
            <img src="{{ $customer->avatar != '' ? asset('storage/images/users/'.$customer->avatar) : asset('img/users/user.jpg')}}">
        </div>
        <div class="user-info py-1">
            <h4>{{ $customer->name.' '.$customer->surname }}</h4>
            <small><strong>{{ $customer->company }}</strong> - {{ $customer->job }}</small>
        </div>
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body pt-0" style="z-index: 1;">
    <div class="row" id="success-alert" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-success animated fadeInDown">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="material-icons">close</i>
                </button>
                <span id="success"></span>
            </div>
        </div>
    </div>
    <div id="cards-accordion" role="tablist" aria-multiselectable="true">
        @isset($cards)
            @forelse($cards as $card)
                <div class="card my-3 animated fadeInDown" style="animation-delay: 0.{{$loop->index + 1}}s">
                    <div class="card-header" role="tab" id="{{ $card->number }}-tab">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" data-parent="#accordianId" href="#{{ $card->number }}-tab-content"
                               aria-expanded="true"
                               aria-controls="section1ContentId">
                                <i class="material-icons align-bottom mr-1">credit_card</i>
                                <strong>{{ $card->number }}</strong>
                                <span class="float-right"><strong>Saldo:</strong> {{ $card->points }} Punti <i
                                            class="material-icons align-middle arrow_drop_down float-right">arrow_drop_down</i></span>
                            </a>
                        </h5>
                    </div>
                    <div id="{{ $card->number }}-tab-content" class="collapse in" role="tabpanel"
                         aria-labelledby="{{ $card->number }}-tab-content">
                        <div class="card-body movements-details pb-0">
                            <h4 class="mb-0 font-weight-bold"><i
                                        class="material-icons align-middle mr-1">compare_arrows</i> Movimenti card</h4>
                            <small>Aggiornati al: {{\Carbon\Carbon::parse(optional($card->movements->last())->created_at)->format('d/m/Y')}}</small>
                            <div class="table-responsive" style="max-height:300px">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Data</th>
                                        <th>Tipologia</th>
                                        <th>Importo</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($card->movements as $movement)
                                        <tr>
                                            <td>{{\Carbon\Carbon::parse($movement->created_at)->format('d/m/Y')}}</td>
                                            <td>{{ $movement->operation->name}} @if($movement->operation->id == 5): {{\Illuminate\Support\Str::limit($movement->coupon->title, 6, $end='...') }} @endif</td>
                                            <td><span>{{ (($movement->type === 'up') ? '+' : '-').$movement->amount }} punti</span><i
                                                        class="material-icons align-middle {{($movement->type === 'up') ? 'movement-up' : 'movement-down' }} float-right">{{($movement->type === 'up') ? 'arrow_drop_up' : 'arrow_drop_down' }}</i>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center font-weight-bold animated fadeIn"><i class="material-icons align-bottom">notification_important</i> Nessun movimento per questa card</p>
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <a href="#" class="btn btn-danger btn-link mt-0 mb-2 w-100 sweetalert-confirm-btn" data-ajax="#modal_content"
                           data-action="{{ route('card.destroy', $card) }}" data-method="delete">
                            <i class="material-icons align-middle mr-1">close</i>
                            <strong>Elimina card</strong>
                        </a>
                    </div>
                </div>
            @empty
                <p id="no-cards" class="text-center font-weight-bold animated fadeInDown"><i
                            class="material-icons align-bottom">notification_important</i> Nessuna card associata al
                    cliente</p>
            @endforelse
        @endisset
        <div id="card-operations-box" class="animated fadeInDown pb-2"
             style="animation-delay: 0.{{$cards->count() + 1}}s; position: relative;
                     z-index: 9999;">
            <form action="{{route('card.store')}}" method="POST" id="card-form" data-ajax="#modal_content" validate>
                @csrf
                @method('POST')
                <h5 class="mt-0 mb-0 btn btn-primary btn-round mx-auto d-block" style="width:fit-content" onclick="
                $(this).fadeOut(200);
                $('#no-cards').fadeOut(200);
                $('.form-add').delay(400).fadeIn(200);">
                    <a href="#">
                        <i class="material-icons align-middle mr-1">add</i>
                        <strong>Aggiungi nuova card</strong>
                    </a>
                </h5>
                <div class="row form-add" style="display: none;">
                    <h4 class="col-12 mb-2 font-weight-bold">Nuova card</h4>
                    <div class="col-md-6">
                        <label>{{ __('Numero card') }}</label>
                        <div class="form-group{{ $errors->has('number') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}"
                                   name="number" id="input-card" type="number"
                                   placeholder="{{ __('Inserisci il numero della card') }}"
                                   value="{{ old('number') }}"
                                   required="true" aria-required="true"
                                   onchange="$('.card-form-submit').delay(200).fadeIn(200); $('.close-btn').removeClass('btn-primary').addClass('btn-secondary');"
                            />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>{{ __('Punti') }}</label>
                        <div class="form-group{{ $errors->has('points') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('points') ? ' is-invalid' : '' }}"
                                   name="points" id="input-points" type="number"
                                   placeholder="{{ __('Inserisci il numero di punti') }}"
                                   value="{{ old('points', 0) }}"
                                   required="true" aria-required="true"/>
                            @if ($errors->has('company'))
                                <span id="company-error" class="error text-danger"
                                      for="input-company">{{ $errors->first('company') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <input type="hidden" name="customer_id" value="{{$customer->id}}">
            </form>
            <hr>
            @if (count($cards) >= 1)
                <a id="operation-make" href="#" class="btn btn-warning btn-round mt-0 mb-3 mx-auto d-block" style="width:fit-content" onclick="
                $(this).fadeOut(200);
                $('#operation-form').delay(400).fadeIn(200);">
                    <i class="material-icons align-middle mr-1">compare_arrows</i>
                    <strong>Nuova operazione</strong>
                </a>
                <form action="{{route('movement.store')}}" method="POST" id="operation-form" data-sending-email="true" data-ajax="#modal_content"
                      style="display: none;">
                    @csrf
                    @method('POST')
                    <div class="row form-add">
                        <h4 class="col-12 mb-2 font-weight-bold">Nuova operazione</h4>
                        <div class="col-md-6">
                            <div class="form-group" style="margin: 10px 0 0;">
                                <div class="form-check form-check-inline py-2">
                                    <label class="form-check-label mr-2" style="padding-left: 14px; padding-right:5px;">
                                        <input class="form-check-input" type="radio" name="type"
                                               value="up" checked><i class="material-icons movement-up align-middle" style="line-height: 18px; width:18px">arrow_drop_up</i> <strong>Aggiungi</strong>
                                        <span class="circle">
                                          <span class="check"></span>
                                        </span>
                                    </label>
                                    <label class="form-check-label" style="padding-left: 14px; padding-right:5px;">
                                        <input class="form-check-input" type="radio" name="type"
                                               value="down"><i class="material-icons movement-down align-middle" style="line-height: 18px; width:18px">arrow_drop_down</i> <strong>Sottrai</strong>
                                        <span class="circle">
                                          <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control"
                                       name="amount" id="input-amount" type="number"
                                       placeholder="{{ __('Inserisci l\'importo') }}"
                                       value=""
                                       required="true" aria-required="true"
                                       onchange="$('.card-op-form-submit').delay(200).fadeIn(200); $('.close-btn').removeClass('btn-primary').addClass('btn-secondary');"/>
                            </div>
                        </div>
                        <div class="col-md-6 pb-3">
                            <div class="form-group mt-2 mt-md-0">
                                <select class="selectpicker" name="card_id"
                                        data-style="btn btn-primary btn-round px-3 my-0 h-100">
                                    @foreach ($cards as $card)
                                        <option value="{{ $card->id }}"
                                                data-icon="fa fa-credit-card mr-2"> {{ $card->number }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mt-2 mt-md-0">
                                <select class="selectpicker" name="operation_id"
                                        data-style="btn btn-primary btn-round px-3 my-0 h-100">
                                    @foreach ($operations as $operation)
                                        @if($operation->id !== 5)
                                        <option value="{{ $operation->id }}"> {{ $operation->name }} </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="customer_id" value="{{$customer->id}}">
                </form>
                @endif
            {{--@if(count($cards) > 1)
                <a id="transfer-make" href="#" class="btn btn-warning mt-0 w-100" onclick="
                $(this).fadeOut(200);
                $('#transfer-form').delay(400).fadeIn(200);">
                    <i class="material-icons align-middle mr-1">compare_arrows</i>
                    <strong>Trasferimento punti</strong>
                </a>
                <form action="{{route('movement.store')}}" method="POST" id="transfer-form" data-ajax="#modal_content"
                      style="display: none;">
                    @csrf
                    @method('POST')
                    <div class="row form-add">
                        <h4 class="col-12 mb-2 font-weight-bold">Nuovo trasferimento</h4>
                        <input type="hidden" name="type" value="transfer">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control"
                                       name="amount" id="input-amount" type="number"
                                       placeholder="{{ __('Inserisci l\'importo') }}"
                                       value=""
                                       required="true" aria-required="true"
                                       onchange="$('.card-op-form-submit').delay(200).fadeIn(200); $('.close-btn').removeClass('btn-primary').addClass('btn-secondary');"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mt-2 mt-md-0">
                                <select class="selectpicker" name="sender_id"
                                        data-style="btn btn-primary btn-round px-3">
                                    @foreach ($cards as $card)
                                        <option value="{{ $card->id }}"
                                                data-icon="fa fa-credit-card mr-2"> {{ $card->number }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mt-2 mt-md-0">
                                <select class="selectpicker" name="receiver_id"
                                        data-style="btn btn-primary btn-round px-3">
                                    @foreach ($cards as $card)
                                        <option value="{{ $card->id }}"
                                                data-icon="fa fa-credit-card mr-2"> {{ $card->number }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="customer_id" value="{{$customer->id}}">
                </form>
            @endif--}}
        </div>
    </div>
    @if($customer->Coupons()->exists())
        <div id="cards-accordion" role="tablist" aria-multiselectable="true">
            <div class="card my-3 animated fadeInDown" style="animation-delay: 1s">
                <div class="card-header" role="tab">
                    <h5 class="mb-0">
                        <a data-toggle="collapse" href="#prize-requests-tab-content">
                            <i class="material-icons align-bottom mr-1">star</i>
                            <strong>Premi richiesti</strong>
                            <span class="float-right"><i class="material-icons align-middle arrow_drop_down float-right">arrow_drop_down</i></span>
                        </a>
                    </h5>
                </div>
                <div id="prize-requests-tab-content" class="collapse in" role="tabpanel"
                     aria-labelledby="prize-requests-tab-content">
                    <div class="card-body movements-details pb-0">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Data</th>
                                <th>Nome</th>
                                <th>Validazione</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($customer->Coupons as $prize)
                                <tr>
                                    <td>{{\Carbon\Carbon::parse($customer->CouponPivot($prize->id)->created_at)->format('d/m/Y')}}</td>
                                    <td>{{ $prize->title }}</td>
                                    <td id="validation-{{$prize->id}}-{{$customer->id}}">
                                        @if($prize->pivot->accepted)
                                            Validato: <strong>{{ $customer->CouponPivot($prize->id)->accepted }}</strong>
                                        @else

                                            <a href="javascript:void(0)" class="btn btn-primary btn-round"
                                               id="validate-prize"
                                               no-loader
                                               data-prize-name="{{ $prize->title }}"
                                               data-prize="{{ $prize->id }}"
                                               data-customer="{{ $customer->id }}"
                                               data-customer-name="{{ ($customer->name)[0].'. '.$customer->surname.' - #'. App\Movement::where([['user_id',$customer->id], ['coupon_id', $prize->id]])->first()->card->number }}"
                                               data-url="{{ route('validate-prize', ['user' => $customer->id, 'prize' => $prize->id]) }}"
                                            onclick="typeof validatePrize == 'function' && validatePrize(event)">Valida</a>
                                            @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">
                                        <p class="text-center font-weight-bold animated fadeIn"><i class="material-icons align-bottom">notification_important</i> Nessun movimento per questa card</p>
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-primary btn-round card-op-form-submit mr-3" form="operation-form" style="display:none;">
        ESEGUI OPERAZIONE
    </button>
    <button type="submit" class="btn btn-primary btn-round card-form-submit mr-3" form="card-form" style="display:none;">CREA
        NUOVA CARD
    </button>
    <button type="button" class="btn btn-primary btn-round close-btn" data-dismiss="modal">Chiudi</button>
</div>