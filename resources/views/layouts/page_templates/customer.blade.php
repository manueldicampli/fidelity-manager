<div class="wrapper customer">
    <div class="main-panel">
        @include('layouts.navbars.navs.customer')
        @yield('content')
    </div>
</div>
@include('layouts.footers.auth')