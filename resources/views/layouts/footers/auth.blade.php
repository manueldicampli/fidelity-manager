<footer class="footer">
  <div class="container">
    <strong class="float-left mb-0">Fidelity Manager</strong>
    <div class="copyright">
      &copy;
      {{ date('Y') }}
      <a style="color:#3c4858" href="https://www.hotelpromenadeabruzzo.it/" target="_blank">Cozzolino Hotels</a>
    </div>
  </div>
</footer>