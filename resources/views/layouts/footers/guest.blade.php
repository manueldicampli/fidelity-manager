<footer class="footer">
    <div class="container">
        <nav class="float-left">
        <ul>
            <li>
            <a class="logo" href="https://www.hotelpromenadeabruzzo.it" target="_blank">
                <img src="{{ asset('img/hotelpromenade-logo.png')}}">
                {{ __('Hotel Promenade') }}
            </a>
            </li>
            <li>
            <a href="https://www.hotelpromenadeabruzzo.it/hotel-pescara-all-inclusive-4-stelle/" target="_blank">
                {{ __('Hotel') }}
            </a>
            </li>
            <li>
            <a href="https://www.hotelpromenadeabruzzo.it/offerte-mare-hotel-4-stelle-motesilvano-abruzzo/" target="_blank">
                {{ __('Offers') }}
            </a>
            </li>
            <li>
            <a href="https://www.hotelpromenadeabruzzo.it/abruzzo-hotel-4-stelle-sul-mare/" target="_blank">
                {{ __('Contacts') }}
            </a>
            </li>
        </ul>
        </nav>
        <div class="copyright">
        &copy;
        {{ date('Y') }}
        <a style="color:#3c4858" href="https://www.hotelpromenadeabruzzo.it/" target="_blank">Hotel Promenade</a>
        </div>
    </div>
</footer>