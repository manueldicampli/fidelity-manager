<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent">
    <div class="container">
        <div class="navbar-wrapper d-flex w-100">
            <a class="navbar-brand logo ml-0" href="#"><img src="{{ asset('img/icon.png')}}"></a>
            @if(Route::currentRouteName() !== 'home')
            <div class="profile-info nav-profile mr-4">
                <div class="profile-photo shadow mr-2"
                     style="background-image: url('{{ auth()->user()->avatar != '' ? asset('storage/images/users/'.auth()->user()->avatar) : asset('img/users/user.jpg')}}')" data-toggle="modal"
                     data-target="#upload_modal">
                </div>
                <div class="profile-name mr-4">
                    <h2>{{ auth()->user()->name }}</h2>
                </div>
                <div class="user-points shadow rounded ml-auto my-auto p-2">
                    <i class="material-icons d-inline-block">star</i>
                    <p class="d-inline-block"><strong>{{ $user_points }}</strong> Punti</p>
                </div>
            </div>
            @endif
            <a class="navbar-item d-inline-block p-0 btn btn-primary btn-fab btn-round @if(Route::currentRouteName() === 'home') ml-auto @endif" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="material-icons">exit_to_app</i></a>
        </div>
    </div>
</nav>