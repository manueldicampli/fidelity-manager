<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent">
  <div class="container-fluid">
    <div class="navbar-wrapper d-flex justify-content-center w-100">
      <a class="navbar-brand logo font-weight-bold d-flex align-items-center" href="{{route('login')}}" no-loader><img src="{{ asset('img/icon.png')}}"> <span>Cozzolino Hotels</span></a>
    </div>
  </div>
</nav>