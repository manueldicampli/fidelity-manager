<div class="sidebar" data-color="azure" data-background-color="white">
    <div class="logo">
        <a href="{{route('home')}}" class="simple-text text-center logo-normal">
            <img src="{{asset('img')}}/icon.png">
            <span class="d-inline-block mt-2">FIDELITY MANAGER</span>
        </a>
    </div>

    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{ auth()->user()->avatar != '' ? asset('storage/images/users/'.auth()->user()->avatar) : asset('img/users/user.jpg')}}">
            </div>
            <div class="user-info">
                <a class="d-inline-block pl-2" href="{{ route('profile.edit') }}">
                    {{ auth()->user()->name.' '.auth()->user()->surname }}
                </a>
                <a class="d-inline-block p-0" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="material-icons">exit_to_app</i></a>
            </div>
        </div>
        <ul class="nav">
            <li class="nav-item{{ $activePage == 'home' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="material-icons">dashboard</i>
                    <p>{{ __('Home') }}</p>
                </a>
            </li>
            <li class="nav-item {{ $activePage == 'coupon' || $activePage == 'coupon_categories' ? 'active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#coupon">
                    <i class="material-icons">local_offer</i>
                    <p> Premi
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse dd {{ $activePage == 'coupon' || $activePage == 'coupon_categories' ? 'show' : '' }}" id="coupon">
                    <ul class="nav">
                        <li class="nav-item{{ $activePage == 'coupon' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('coupon.index') }}">
                                <i class="material-icons">list</i>
                                <p>{{ __('Lista Premi') }}</p>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'coupon_categories' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('category.index') }}">
                                <i class="material-icons">category</i>
                                <p>{{ __('Categorie Premi') }}</p>
                            </a>
                        </li>
                        <hr>
                    </ul>
                </div>
            </li>
            @if(auth()->user()->isAdmin())
                <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('user.index') }}">
                        <i class="material-icons">person</i>
                        <p>{{ __('Gestione Utenti') }}</p>
                    </a>
                </li>
                <li class="nav-item{{ $activePage == 'customers-management' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('customers.index') }}">
                        <i class="material-icons">person</i>
                        <p>{{ __('Gestione Clienti') }}</p>
                    </a>
                </li>
                <li class="nav-item{{ $activePage == 'operations-management' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('operations.index') }}">
                        <i class="material-icons">settings</i>
                        <p>{{ __('Gestione Operazioni') }}</p>
                    </a>
                </li>
                <li class="nav-item{{ $activePage == 'movements-management' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('movement.index') }}">
                        <i class="material-icons">compare_arrows</i>
                        <p>{{ __('Movimenti') }}</p>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>