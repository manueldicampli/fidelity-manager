@extends('layouts.app', ['activePage' => '', 'class' => 'customer-movements frontend', 'title' => 'Movimenti'])

@section('content')
    <div class="content mt-0">
        <div class="container">
            <section id="movements">
                <div class="movements-heading heading d-sm-flex">
                    <h3 class="my-0">Tutti i movimenti</h3>
                    <a href="{{ route('home') }}" class="all-movements d-inline-block ml-auto" style="line-height: 37px;"><i
                                class="material-icons">home</i> Torna alla home</a>
                </div>
                <ul class="movements-list">
                    @forelse($movements as $movement)
                        <li class="movements-item shadow rounded">
                            <div class="d-block d-md-flex px-3 @if($movement->operation->id == 5) align-items-center py-3 @else py-4 @endif" style="pointer-events: none">
                                <div class="movements-item-date movements-item-meta mr-3 mb-3 mb-md-0">
                                    <i class="material-icons">calendar_today</i>
                                    <span>{{\Carbon\Carbon::parse($movement->created_at)->format('d/m/Y')}}</span>
                                </div>
                                <div class="movements-item-operation-name movements-item-meta mr-3 mb-3 mb-md-0">
                                    <i class="material-icons">
                                        @switch($movement->operation->name)
                                            @case('RISTORANTE')
                                            restaurant
                                            @break
                                            @case('BENESSERE E RELAX')
                                            spa
                                            @break
                                            @case('BAR')
                                            local_cafe
                                            @break
                                            @case('PERNOTTAMENTO')
                                            king_bed
                                            @break
                                            @default
                                            category
                                        @endswitch
                                    </i>
                                    <span>@if($movement->partial)(PAR) @endif{{ $movement->operation->name}} @if($movement->operation->id == 5): {{\Illuminate\Support\Str::limit($movement->coupon->title, 15, $end='...') }} @endif</span>
                                </div>
                                <div class="movements-item-location movements-item-meta mr-3 mb-3 mb-md-0">
                                    <i class="material-icons">location_on</i>
                                    <span>{{$movement->location}}</span>
                                </div>
                                @if($movement->operation->id == 5)
                                <div class="d-none d-md-block movements-item-location movements-item-meta mr-3 mb-3 mb-md-0 btn btn-primary btn-fab btn-round ml-auto">
                                    <a class="d-block h-100" data-toggle="tooltip" data-trigger="hover" style="pointer-events:all" title="Visualizza richiesta" href="{{ route('frontend.prize-status', $movement->coupon->slug) }}"><i class="material-icons" style="color:#FFF!important; font-size:12px">remove_red_eye</i></a>
                                </div>
                                <div class="d-block d-md-none movements-item-location movements-item-meta mr-3 mb-3 mb-md-0 btn btn-primary btn-round">
                                    <a class="d-block text-light" style="pointer-events:all" title="Visualizza richiesta" href="{{ route('frontend.prize-status', $movement->coupon->slug) }}">VISUALIZZA RICHIESTA</a>
                                </div>
                                @endif
                                <div class="movements-item-points-amount movements-item-meta @if(!($movement->operation->id == 5)) ml-auto @endif">
                                    <i class="material-icons align-middle {{($movement->type === 'up') ? 'movement-up' : 'movement-down' }}">
                                        {{($movement->type === 'up') ? 'arrow_drop_up' : 'arrow_drop_down' }}</i>
                                    <span class="font-weight-bold">{{ (($movement->type === 'up') ? '+' : '-').$movement->amount }} punti</span>
                                </div>
                            </div>
                        </li>
                    @empty
                        <p>Nessun movimento effettuato.</p>
                    @endforelse
                </ul>
                {{ $movements->links() }}
            </section>
        </div>
    </div>
@endsection