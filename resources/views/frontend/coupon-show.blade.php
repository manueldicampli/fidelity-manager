@extends('layouts.app', ['activePage' => 'coupon', 'class' => 'show frontend', 'title' => $title])
@section('content')
    @guest
        @include('layouts.navbars.navs.guest')
    @endguest
    <div class="content mt-0">
        <div class="container">
            <div class="frontend-single-heading d-lg-flex justify-content-lg-between align-items-lg-center">
                <h3 class="mt-0"><a class="btn btn-primary btn-fab btn-fab-mini btn-round" href="{{ route('home') }}"
                                    data-toggle="tooltip" data-placement="top" title="Torna alla Home"><i
                                class="material-icons">arrow_back</i></a><span>{{ $coupon->title }}</span></h3>
                <div class="coupon-badges d-sm-flex">
                    <span class="label label-inv mr-2 mb-2"><i class="material-icons">star</i><span>{{ $coupon->value }} punti</span></span>
                    <span class="label label-inv mr-2 mb-2"><i
                                class="material-icons">{{ $coupon->category->icon ?? 'category' }}</i><span>{{ $category_name }}</span></span>
                    <span class="label label-inv mb-2"><span>{{ $coupon->location }}</span></span>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-md-6 order-md-12">
                    <div class="coupon-img mb-5 mb-md-5">
                        <img class="img my-0" src="@if($coupon->img == 'coupon_img.png'){{ asset('img/coupons/coupon_img.png')}}@else{{ asset('storage/images/coupons/'.$coupon->img)}}@endif">
                    </div>
                </div>
                <div class="col-md-6 order-md-1">
                    <div class="info-coupon d-flex flex-column h-100">
                        <div class="description">
                            <h3 class="mt-0"><i class="material-icons mr-2">subject</i>Descrizione premio</h3>
                            {!! $coupon->description !!}
                        </div>
                        <div class="deadline mb-5 mb-md-0">
                            <h3><i class="material-icons mr-2">date_range</i>Data Scadenza</h3>
                            <h4>{{\Carbon\Carbon::parse($coupon->deadline)->format('d/m/Y')}}</h4>
                        </div>
                        <div class="d-flex justify-content-center justify-content-sm-start flex-column flex-sm-row">
                        <span style="width: fit-content; width: -moz-fit-content" class="mt-auto mx-auto ml-sm-0 mr-sm-0" @if($user_points < $coupon->value && !Auth::user()->Coupons->contains($coupon))
                              data-toggle="tooltip" title="Per richiedere questo premio hai bisogno {{ ($coupon->value - $user_points) === 1 ? 'ancora di un altro' : 'di altri ' }}{{ ($coupon->value - $user_points) === 1 ? '' : $coupon->value - $user_points }} punt{{ ($coupon->value - $user_points) === 1 ? 'o' : 'i' }}"
                           @endif>
                            @if(count(Auth::user()->cards) > 1)
                            <button type="button" class="btn btn-primary btn-round @if($user_points < $coupon->value || Auth::user()->Coupons->contains($coupon)) disabled @endif" data-toggle="modal" data-target="#prizeRequestModal" id="requestBtn" no-loader>
                               @if(Auth::user()->Coupons->contains($coupon))
                                    <i class="fa fa-check mr-2"></i> Premio richiesto il <strong>{{ \Carbon\Carbon::parse(Auth::user()->CouponPivot($coupon->id)->created_at)->format('d/m/Y') }}</strong>
                                @else
                                    <i class="fa fa-gift mr-2"></i>
                                    Richiedi premio
                                @endif
                           </button>
                                @else
                                <a href="javascript:void(0)" data-ajax data-action="{{ route('frontend.request-prize', $coupon) }}" data-method="post" class="btn btn-primary btn-round @if($user_points <= $coupon->value || Auth::user()->Coupons->contains($coupon)) disabled d-inline-block mx-auto mr-sm-3 ml-sm-0 @endif" id="requestBtn" data-prize-request="true" no-loader>
                               @if(Auth::user()->Coupons->contains($coupon))
                                        <i class="fa fa-check mr-2"></i> Premio richiesto
                                    @else
                                        <i class="fa fa-gift mr-2"></i>
                                        Richiedi premio
                                    @endif
                           </a>
                            @endif
                        </span>
                        @if(Auth::user()->Coupons->contains($coupon))
                        <a href="{{ route('frontend.prize-status', $coupon->slug) }}" class="btn btn-primary btn-round d-inline-block ml-sm-0 mx-auto">
                               VISUALIZZA RICHIESTA
                           </a>
                           @endif
                           </div>

                    </div>
                </div>
            </div>

            @if(count($related_coupons) >= 1)
            <div class="frontend-related-prizes">
                <section id="premi">
                    <div class="heading">
                        <h3>Altri premi</h3>
                    </div>
                    <div class="premi-slider">
                        <div class="swiper-container mb-4">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                @foreach($related_coupons as $related_coupon)
                                    <div class="swiper-slide">
                                        <div class="card card-product animated fadeInRight">
                                            <div class="card-header card-header-image">
                                                <a href="{{ route('frontend.show-coupon', $related_coupon->slug) }}">
                                                    <span class="badge shadow badge-pill badge-primary">{{ $related_coupon->location }}</span>
                                                    <img class="img" src="@if($related_coupon->img == 'coupon_img.png'){{ asset('img/coupons/coupon_img.png')}}@else{{ asset('storage/images/coupons/'.$related_coupon->img)}}@endif">
                                                </a>
                                            </div>
                                            <div class="card-body">
                                                <h4 class="card-title">
                                                    <a href="{{ route('frontend.show-coupon', $related_coupon->slug) }}">{{ $related_coupon->title }}</a>
                                                </h4>
                                                <div class="card-description">
                                                    {!! $related_coupon->description !!}
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                @if(!Auth::user()->coupons->contains($related_coupon))
                                                <div class="stats">
                                                    <p class="card-category"><i class="material-icons">star</i> {{ $related_coupon->value }} punti</p>
                                                </div>
                                                <div class="stats">
                                                    <p class="card-category">
                                                        <i class="material-icons"> {{ $related_coupon->Category->icon }}</i>
                                                        {{ $related_coupon->Category->name }}
                                                    </p>
                                                </div>
                                                    @else
                                                    <div class="stats">
                                                        <p class="card-category"><i class="material-icons">done</i> Richiesto il <strong>{{ \Carbon\Carbon::parse(Auth::user()->CouponPivot($related_coupon->id)->created_at)->format('d/m/Y') }}</strong></p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @if(count($related_coupons) > 1)
                        <div class="arrows d-block">
                            <span class="arrow-left d-inline-block mr-3">
                                <i class="fa fa-caret-left"></i>
                            </span>
                            <span class="arrow-right d-inline-block">
                                <i class="fa fa-caret-right"></i>
                            </span>
                        </div>
                        @endif
                    </div>
                </section>
            </div>
                @endif
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="prizeRequestModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header mb-4">
                    <h5 class="modal-title">Richiesta premio: <span class="font-weight-normal">{{ $coupon->title }}</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="prize-points mb-4">
                        <span id="prizeValue" data-value="{{ $coupon->value }}">
                        {{ $coupon->value }}
                        </span>
                        <span>punti ancora da scalare</span>
                    </div>
                    <div class="cards-container mb-5">
                        <strong class="d-block mb-3">Seleziona la card su cui effettuare la transazione</strong>
                        <div id="goBackToCards" class="mb-3"><button class="btn btn-fab btn-round mr-3"><i class="material-icons">apps</i></button><span>Torna indietro</span></div>
                        <div class="cards-wrapper">
                            <div class="row">
                            @foreach(Auth::user()->Cards as $card)
                                <div class="col-sm-4 card-box-wrapper">
                                    <div class="card-box mb-4" data-points="{{$card->points}}" data-number="{{$card->number}}" data-transaction="false">
                                        <div class="card-icon"><i class="material-icons align-bottom mr-1">credit_card</i></div>
                                        <span id="{{$card->number}}" class="mb-2">{{ $card->number }}</span>
                                        <small><strong>Disponibilità:</strong><br> <div id="{{$card->number}}-points" class="d-inline-block">{{ $card->points }}</div> punti</small>
                                    </div>
                                </div>
                            @endforeach
                                <div class="col-12 card-action-box-wrapper">
                                    <div class="card-action-box">
                                        <div class="card-info d-sm-flex justify-content-between">
                                            <div class="card-name">
                                                <div class="card-icon d-inline-block"><i class="material-icons align-bottom mr-1">credit_card</i></div>
                                                <span id="card-number" class="d-inline-block"></span>
                                            </div>
                                            <small id="card-points"></small>
                                        </div>
                                        <div class="form-group label-floating">
                                            <div class="input-group align-items-end">
                                                <input type="text" class="form-control" placeholder="Importo da addebitare">
                                                <span class="input-group-btn">
                                                    <button id="doAction" type="button" class="btn btn-fab btn-round btn-primary disabled" data-card="">
                                                        <i class="material-icons">done</i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-modal mr-3" data-dismiss="modal">Chiudi</button>
                    <button id="confirm" type="button" class="btn btn-primary disabled" data-action="{{route('frontend.request-prize', $coupon)}}">Conferma richiesta</button>
                </div>
            </div>
        </div>
    </div>
    @guest
        @include('layouts.footers.guest')
    @endguest

@endsection