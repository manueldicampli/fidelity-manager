@extends('layouts.app', ['activePage' => '', 'class' => 'prize-status frontend', 'title' => 'Movimenti'])

@section('content')
    <div class="content mt-0">
        <div class="container">
            <section id="movements">
                <div class="movements-heading heading d-sm-flex">
                    <h3 class="my-0">Stato della richiesta per il premio: {{$prize->title}}</h3>
                    <a href="{{ route('home') }}" class="all-movements d-inline-block ml-auto" style="line-height: 37px;"><i
                                class="material-icons">home</i> Torna alla home</a>
                </div>
                <ul class="movements-list">
                        <li class="movements-item shadow rounded @if(!$prize_request_status->accepted) mb-5 @endif">
                            <a href="#" class="d-block d-sm-flex px-3 py-4" style="pointer-events: none">
                                <div class="movements-item-date movements-item-meta mr-3 mb-3 mb-sm-0">
                                    <i class="material-icons">calendar_today</i>
                                    <span>{{\Carbon\Carbon::parse($prize_request_status->created_at)->format('d/m/Y')}}</span>
                                </div>
                                <div class="movements-item-operation-name movements-item-meta mr-3 mb-3 mb-sm-0">
                                    <i class="material-icons" style="color: #38c172">done</i>
                                    <span class="font-weight-bold">Il premio è stato richiesto correttamente</span>
                                </div>
                                <div class="movements-item-location movements-item-meta mr-3 mb-3 mb-sm-0">
                                    <i class="material-icons">location_on</i>
                                    <span>{{$prize->location}}</span>
                                </div>
                                <div class="movements-item-points-amount movements-item-meta ml-auto">
                                    <i class="material-icons align-middle movement-down">arrow_drop_down</i>
                                    <span class="font-weight-bold">-{{$prize->value}} punti</span>
                                </div>
                            </a>
                        </li>
                        @if($prize_request_status->accepted)
                        <li class="movements-item shadow rounded">
                            <a href="#" class="d-block d-sm-flex px-3 py-4" style="pointer-events: none">
                                <div class="movements-item-date movements-item-meta mr-3 mb-3 mb-sm-0">
                                    <i class="material-icons">calendar_today</i>
                                    <span>{{\Carbon\Carbon::parse($prize_request_status->updated_at)->format('d/m/Y')}}</span>
                                </div>
                                <div class="movements-item-operation-name movements-item-meta mr-3 mb-3 mb-sm-0">
                                    <i class="material-icons" style="color: #38c172">verified</i>
                                    <span class="font-weight-bold">Il premio è stato validato dalla struttura</span>
                                </div>
                                <div class="movements-item-location movements-item-meta mr-3 mb-3 mb-sm-0">
                                    <i class="material-icons">location_on</i>
                                    <span>{{$prize->location}}</span>
                                </div>
                            </a>
                        </li>
                        <li class="movements-item shadow rounded">
                            <a href="#" class="d-block d-sm-flex align-items-center flex-column flex-sm-row px-3 py-4" style="pointer-events: none">
                                <h3 class="my-0 mr-3">Il tuo codice di validazione è:</h2>
                                <h2 class="my-0 font-weight-bold">{{ $prize_request_status->accepted }}</h2>
                            </a>
                        </li>
                        @else 
                        <h3 class="font-weight-bold text-center text-sm-left">Grazie per aver richiesto il tuo premio Fidelity!</h3>
                        <p class="text-center text-sm-left mb-4 mb-sm-5">Ti lasciamo qui di seguito le modalità per <span style="color: #d29836; font-weight: bold">contattare la struttura e concordare il ritiro / prenotazione del tuo premio.</p>
                         <div class="d-flex flex-column flex-sm-row justify-content-center justify-content-sm-start align-items-center mb-4">
                            <img align="center" alt="" class="icon mr-sm-4 mb-3 mb-sm-0" height="32" src="{{ asset('img/icons/location_pin.png') }}"style="border:0;" width="null">
                            <strong class="font-weight-bold mr-sm-2">{{$prize->location}}</strong>
                            @if($prize->location == 'Hotel Promenade')
                                <p class="mb-0 text-center text-sm-left">Via Aldo Moro, 63 - Montesilvano Spiaggia (PE) 65013</p>
                            @else
                                <p class="mb-0 text-center text-sm-left">C.da Santa Calcagna, 71/72 - Rocca San Giovanni (CH) 66020</p>
                            @endif
                        </div>
                        <div class="d-flex flex-column flex-sm-row justify-content-center justify-content-sm-start align-items-center mb-4">
                            <img align="center" alt="" class="icon mr-sm-4 mb-3 mb-sm-0" height="32" src="{{ asset('img/icons/phone-call.png') }}"style="border:0;" width="null">
                            <strong class="font-weight-bold">Tel: @if($prize->location == 'Hotel Promenade') 085 445 2221 @else 0872 717645 @endif</strong>
                        </div>
                        <div class="d-flex flex-column flex-sm-row justify-content-center justify-content-sm-start align-items-center mb-5">
                            <img align="center" alt="" class="icon mr-sm-4 mb-3 mb-sm-0" height="32" src="{{ asset('img/icons/email.png') }}"style="border:0;" width="null">
                            <strong class="font-weight-bold">Email:@if($prize->location == 'Hotel Promenade') <a href="mailto:info@hotelpromenadeabruzzo.it">info@hotelpromenadeabruzzo.it</a> @else <a href="mailto:info@hotelvillamediciabruzzo.it">info@hotelvillamediciabruzzo.it</a> @endif</strong>
                        </div>
                        <strong class="font-weight-bold" style="font-size: 20px; text-decoration: underline">Ti ricordiamo che il premio che hai richiesto sarà valido solo previa attivazione da parte di Cozzolino Hotels.</strong>
                        @endif
                </ul>
            </section>
        </div>
    </div>
@endsection