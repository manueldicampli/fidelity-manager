@extends('layouts.app', ['activePage' => 'profile', 'title' => __('My Profile')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('profile.update') }}" autocomplete="off"
                          class="form-horizontal">
                        @csrf
                        @method('put')
                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Edit Profile') }}</h4>
                                <p class="card-category">{{ __('User information') }}</p>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   name="name" id="input-name" type="text"
                                                   placeholder="{{ __('Name') }}"
                                                   value="{{ old('name', auth()->user()->name) }}" required="true"
                                                   aria-required="true"/>
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger"
                                                      for="input-name">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Surname') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('surname') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}"
                                                   name="surname" id="input-surname" type="text"
                                                   placeholder="{{ __('Surname') }}"
                                                   value="{{ old('surname', auth()->user()->surname) }}" required="true"
                                                   aria-required="true"/>
                                            @if ($errors->has('surname'))
                                                <span id="surname-error" class="error text-danger"
                                                      for="input-surname">{{ $errors->first('surname') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Email') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                   name="email" id="input-email" type="email"
                                                   placeholder="{{ __('Email') }}"
                                                   value="{{ old('email', auth()->user()->email) }}" required/>
                                            @if ($errors->has('email'))
                                                <span id="email-error" class="error text-danger"
                                                      for="input-email">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label mb-3">Avatar</label>
                                    <div class="col-sm-7 mb-3">
                                        <div id="preview-img" class="float-left mr-3">
                                            <img class="profile-img"
                                                 src="{{ auth()->user()->avatar != '' ? asset('storage/images/users/'.auth()->user()->avatar) : asset('img/users/user.jpg')}}">
                                        </div>
                                        <button type="button" class="btn btn-primary btn-round mb-3" data-toggle="modal"
                                                data-target="#upload_modal" id="modal_toggle"><i
                                                    class="material-icons mr-2" style="font-size:16px;">cached</i>Change
                                            Image
                                        </button>
                                    </div>
                                    <input name="avatar" value="{{ auth()->user()->avatar }}" hidden>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('profile.password') }}" class="form-horizontal">
                        @csrf
                        @method('put')

                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Change password') }}</h4>
                                <p class="card-category">{{ __('Password') }}</p>
                            </div>
                            <div class="card-body ">
                                @if (session('status_password'))
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="alert alert-success">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                    <i class="material-icons">close</i>
                                                </button>
                                                <span>{{ session('status_password') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <label class="col-sm-2 col-form-label"
                                           for="input-current-password">{{ __('Current Password') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}"
                                                   input type="password" name="old_password" id="input-current-password"
                                                   placeholder="{{ __('Current Password') }}" value="" required/>
                                            @if ($errors->has('old_password'))
                                                <span id="name-error" class="error text-danger"
                                                      for="input-name">{{ $errors->first('old_password') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label"
                                           for="input-password">{{ __('New Password') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                   name="password" id="input-password" type="password"
                                                   placeholder="{{ __('New Password') }}" value="" required/>
                                            @if ($errors->has('password'))
                                                <span id="password-error" class="error text-danger"
                                                      for="input-password">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label"
                                           for="input-password-confirmation">{{ __('Confirm New Password') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" name="password_confirmation"
                                                   id="input-password-confirmation" type="password"
                                                   placeholder="{{ __('Confirm New Password') }}" value="" required/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">{{ __('Change password') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade filter" id="upload_modal" tabindex="-1" role="dialog" aria-labelledby="upload_modal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="upload_modal">Upload Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="upload-demo"></div>
                    <div class="d-flex justify-content-center">
                        <label class="btn btn-primary btn-round" for="image_file">
                            <input id="image_file" type="file" style="display:none"
                                   onchange="$('#upload-file-info').html(this.files[0].name)">
                            Choose Image
                        </label>
                    </div>
                    <div class="d-flex justify-content-center">
                        <span class='label label-info' id="upload-file-info"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary mr-2" data-dismiss="modal">Close</button>
                    <button id="upload_btn" class="btn btn-primary upload-image disabled" data-dismiss="modal">Upload
                        Image
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script type="text/javascript">
        //IMAGE_UPLOAD
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var resize = $('#upload-demo').croppie({
            enableExif: true,
            enableOrientation: true,
            viewport: { // Default { width: 100, height: 100, type: 'square' }
                width: 200,
                height: 200,
                type: 'circle' //square
            },
            boundary: {
                width: 300,
                height: 300
            }
        });
        var extension = "";
        $('#image_file').on('change', function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                resize.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                    console.log('jQuery bind complete');
                });
            };
            var re = /(?:\.([^.]+))?$/;
            filename = $(this).val().replace(/^.*[\\\/]/, '');
            extension = re.exec(filename)[1];
            console.log(extension);
            reader.readAsDataURL(this.files[0]);
            $('#upload_btn').removeClass('disabled');
        });
        $('.upload-image').on('click', function (ev) {
            ev.preventDefault();
            resize.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (img) {
                var html = '<img class="profile-img" src="' + img + '" />';
                $('.sidebar .photo img').attr('src', img);
                $("#preview-crop-image").html(html);
                $("#preview-img").html(html);
                $.ajax({
                    url: "/crop-image",
                    type: "POST",
                    data: {
                        "image": img,
                        "extension": extension
                    },
                    success: function (data) {
                        var filename = data.img;
                        $('input[name="avatar"]').val(filename);
                        let button_html = '<i class="material-icons mr-2" style="font-size:16px;">cached</i>Change Image';
                        $.notify({
                            icon: "done",
                            message: "Immagine profilo aggiornata con successo!"

                        }, {
                            type: 'success',
                            timer: 2000,
                            placement: {
                                from: 'bottom',
                                align: 'right'
                            }
                        });
                        $('#modal_toggle').html(button_html);
                    },
                    error: function (response) {
                        console.log(response);
                    }
                });
            });
        });

        @if(session('status'))
        $.notify({
            icon: "done",
            message: "{{ session('status') }}"

        }, {
            type: 'success',
            timer: 2000,
            placement: {
                from: 'bottom',
                align: 'right'
            }
        });
        @endif

    </script>
@endpush