<?php

use Illuminate\Database\Seeder;
use App\Coupon;

class CouponsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Coupon::create([
            'title' => 'Soggiorno per due persone',
            'slug' => "soggiorno-per-due-persone",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                              sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'deadline' => Date("m/d/Y", strtotime("+3 days")),
            'location' => 'Hotel Promenade',
            'value' => 490,
            'availability' => 'available',
            'img' => 'coupon_img.png',
            'category_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Coupon::create([
            'title' => 'Premio 2',
            'slug' => "premio-2",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                              sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'deadline' => Date("m/d/Y", strtotime("+3 days")),
            'location' => 'Hotel Villa Medici',
            'value' => 600,
            'availability' => 'not_available',
            'img' => 'coupon_img.png',
            'category_id' => 2,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Coupon::create([
            'title' => 'Premio 3',
            'slug' => "premio-3",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                              sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                              Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'deadline' => Date("m/d/Y", strtotime("+3 days")),
            'location' => 'Hotel Villa Medici',
            'value' => 700,
            'availability' => 'available',
            'img' => 'coupon_img.png',
            'category_id' => 3,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
