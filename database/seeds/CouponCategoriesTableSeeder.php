<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CouponCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coupon_categories')->insert(
            ['name' => 'Benessere e Relax',
            'icon' => 'spa',
            'created_at' => now(),
            'updated_at' => now()]
        );
        DB::table('coupon_categories')->insert(
            ['name' => 'Ristorante',
            'icon' => 'restaurant',
            'created_at' => now(),
            'updated_at' => now()]
        );
        DB::table('coupon_categories')->insert(
            ['name' => 'Pernottamento',
            'icon' => 'king_bed',
            'created_at' => now(),
            'updated_at' => now()]
        );
    }
}
