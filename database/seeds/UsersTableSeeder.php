<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Manuel',
                'surname' => 'Di Campli',
                'city' => 'Pescara',
                'gender' => 'M',
                'phone' => '3735343527',
                'company' => 'Hotel Promenade',
                'job' => 'Web Developer',
                'birthday' => '20/11/1996',
                'email' => 'admin@manueldicampli.it',
                'email_verified_at' => now(),
                'username' => 'manuel',
                'password' => Hash::make('12345678'),
                'avatar' => '',
                'admin' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Mario',
                'surname' => 'Rossi',
                'city' => '68',
                'gender' => 'M',
                'phone' => '3735343527',
                'company' => 'Pomilio Blumm',
                'job' => 'Web Developer',
                'birthday' => '20/11/1996',
                'email' => 'manuel.dicampli@rocketmail.com',
                'email_verified_at' => now(),
                'username' => 'mario',
                'password' => Hash::make('Manuelito96'),
                'avatar' => '',
                'admin' => 0,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Giovanni',
                'surname' => 'Pucci',
                'city' => '53',
                'gender' => 'M',
                'phone' => '3735343527',
                'company' => 'Hotel Promenade',
                'job' => 'Web Developer',
                'birthday' => '20/10/1972',
                'email' => 'chef@hotelpromenadeabruzzo.it',
                'email_verified_at' => now(),
                'username' => 'giovanni',
                'password' => Hash::make('12345678'),
                'avatar' => '',
                'admin' => 0,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Marta',
                'surname' => 'Manieri',
                'city' => '68',
                'gender' => 'F',
                'phone' => '3466360286',
                'company' => 'Hotel Promenade',
                'job' => 'Welcome',
                'birthday' => '28/01/1991',
                'email' => 'marta@manieri.it',
                'email_verified_at' => now(),
                'username' => 'marta',
                'password' => Hash::make('12345678'),
                'avatar' => '',
                'admin' => 0,
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
