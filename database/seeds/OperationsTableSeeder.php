<?php

use App\Operation;
use Illuminate\Database\Seeder;

class OperationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Operation::create([
            'name' => 'PERNOTTAMENTO',
            'ppeuro' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Operation::create([
            'name' => 'RISTORANTE',
            'ppeuro' => 2,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Operation::create([
            'name' => 'BAR',
            'ppeuro' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Operation::create([
            'name' => 'BENESSERE E RELAX',
            'ppeuro' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Operation::create([
            'name' => 'RICHIESTA PREMIO',
            'ppeuro' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Operation::create([
            'name' => 'ALTRO',
            'ppeuro' => 1,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
