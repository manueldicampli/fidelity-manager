<?php

namespace App;

use App\Coupon;
use Illuminate\Database\Eloquent\Model;

class CouponCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'icon'
    ];

    /**
     * Coupon users relationship.
     */
    public function Coupons() {
        return $this->hasMany(Coupon::class);
    }
}
