<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'amount', 'location', 'partial', 'card_id', 'operation_id', 'coupon_id', 'user_id'
    ];

    /**
     * Operation - Movements relationship.
     */
    public function Operation() {
        return $this->belongsTo(Operation::class);
    }

    /**
     * Card - Movements relationship.
     */
    public function Card() {
        return $this->belongsTo(Card::class);
    }

    /**
     * User - Movements relationship.
     */
    public function User() {
        return $this->belongsTo(User::class);
    }

    /**
     * Movement - Coupon relationship.
     */
    public function Coupon() {
        return $this->belongsTo(Coupon::class);
    }

}
