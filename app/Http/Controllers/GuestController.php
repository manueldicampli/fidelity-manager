<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    /**
     * Display a prize if no logged in
     *
     */
    public function showCoupon($slug)
    {
        $coupon = Coupon::where('slug', $slug)->firstOrFail();
        if ($coupon->Category) {
            $category_name = $coupon->category->name;
        } else {
            $category_name = 'Uncategorized';
        }

        $now = date('d/m/Y');

        $title = $coupon->title . ' - ' . $category_name . ' | ' . $coupon->location;

        return view('coupon.show-guest', ['coupon' => $coupon, 'category_name' => $category_name, 'title' => $title]);
    }
}
