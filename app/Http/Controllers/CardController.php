<?php

namespace App\Http\Controllers;

use App\Card;
use App\User;
use App\Operation;
use Illuminate\Http\Request;

class CardController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Card::create([
            'number' => $request->number,
            'points' => ltrim($request->points, '0') ?: '0',
            'user_id' => $request->customer_id
        ]);

        $customer = User::find($request->customer_id);

        $operations = Operation::all();
        $cards = $customer->cards;

        return response()->json(['titolo'=>null,'tipo'=>'modal','messaggio'=>'Card aggiunta con successo!','risultato'=>view('customers.modal', ['customer' => $customer, 'cards' => $cards, 'operations' => $operations])->render()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Card $card)
    {
        $customer = $card->user;
        $card->Movements()->delete();
        $card->delete();

        $cards = $customer->cards;
        $operations = Operation::all();

        return response()->json(['titolo'=>null,'tipo'=>'modal','messaggio'=>'','risultato'=>view('customers.modal', ['customer' => $customer, 'cards' => $cards, 'operations' => $operations])->render()]);
    }
}
