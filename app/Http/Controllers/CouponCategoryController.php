<?php

namespace App\Http\Controllers;

use App\CouponCategory;
use App\Http\Requests\CouponCategoryRequest;
use Illuminate\Http\Request;

class CouponCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\CouponCategory  $model
     * @return \Illuminate\View\View
     */
    public function index(CouponCategory $model)
    {
        return view('coupon.categories.index', ['categories' => $model->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $file_path = public_path('material/material_icons.json');
        $list = json_decode(file_get_contents($file_path), true);
        $icons = []; 

        foreach ($list['icons'] as $i => $data) {
            $icons[$i] = strtolower(str_replace(' ', '_', $data['name']));
        }
        return view('coupon.categories.create', compact('icons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CouponCategoryRequest  $request
     * @param  \App\CouponCategory  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CouponCategoryRequest $request, CouponCategory $model)
    {
        $model->create($request->all());

        return redirect()->route('category.index')->withStatus(__('New category successfully created.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CouponCategory  $coupon_category
     * @return \Illuminate\View\View
     */
    public function edit(CouponCategory $category)
    {
        return view('coupon.categories.edit', compact('category'));
    }

    /**
     * Update the specified coupon category in storage.
     *
     * @param  \App\Http\Requests\CouponCategoryRequest  $request
     * @param  \App\CouponCategory  $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CouponCategoryRequest $request, CouponCategory $category)
    {
        $category->update($request->all());

        return redirect()->route('category.index')->withStatus(__('Coupon category successfully updated.'));
    }

    /**
     * Remove the specified coupon from storage.
     *
     * @param  \App\CouponCategory  $category
     * @return \Illuminate\Http\RedirectResponse
     * 
     */
    public function destroy(CouponCategory $category)
    {
        $category->delete();

        return redirect()->route('category.index')->withStatus(__('Coupon category successfully deleted.'));
    }
}
