<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerPasswordRequest;
use App\Http\Requests\UserRequest;
use App\Operation;
use App\User;
use App\Card;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(User $model)
    {
        $users = $model->where('admin', 0);
        return view('customers.index', ['users' => $users->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $province = DB::table('province')->select('id', 'nome')->orderBy('nome')->get();
        return view('customers.create', compact('province'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request, User $model)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users|max:255',
            'password' => 'required|confirmed|min:6'
        ]);

        if ($validator->fails()) {
            return redirect()->route('customers.create', compact('model'))
                ->withErrors($validator)
                ->withInput();
        }

        $new_customer = $model->create($request->merge(['password' => Hash::make($request->get('password')), 'admin' => 0])->all());

        Card::create([
            'number' => $request->card,
            'points' => $request->points,
            'user_id' => $new_customer->id
        ]);

        return redirect()->route('customers.index')->withStatus(__('Cliente aggiunto con successo.'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(User $customer)
    {
        $provincia = "";
        if($customer->city) {
            $provincia = DB::table('province')->where('id', $customer->city)->first();
            $provincia = $provincia->nome;
        }
        return response()->json(['titolo'=>null,'tipo'=>'modal','messaggio'=>null,'risultato'=>view('customers.show', compact('customer', 'provincia'))->render()]);
    }

    /**
     * Get customer's cards list.
     * @param User $customer
     */
    public function cards(User $customer)
    {
        $cards = $customer->Cards()->get();
        $operations = DB::table('operations')->select('*')->get();
        return response()->json(['titolo'=>null,'tipo'=>'modal','messaggio'=>null,'risultato'=>view('customers.modal', compact('customer','cards', 'operations'))->render()]);
    }

    /**
     * Show the form for editing the specified user
     *
     * @param \App\User $user
     * @return \Illuminate\View\View
     */
    public function edit(User $customer)
    {
        $province = DB::table('province')->select('id', 'nome')->orderBy('nome')->get();
        return view('customers.edit', compact('customer', 'province'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function update(Request $request, User $customer)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users|max:255'
        ]);

        if ($validator->fails()) {
            return redirect()->route('customers.edit', compact('customer'))
                ->withErrors($validator)
                ->withInput();
        }

        if($request->ajax()) {
            if($request->avatar != auth()->user()->avatar){
                if($request->avatar !== 'user.jpg'){
                    File::move('img/tempdir/'.$request->avatar, 'img/users/'.$request->avatar);
                }
                $file = new Filesystem;
                $file->cleanDirectory(public_path('img/tempdir/'));
            }
            auth()->user()->update($request->all());
            return response()->json(['titolo'=>null,'tipo'=>null,'messaggio'=>null,'risultato' => $request]);
        }

        $customer->update($request->all());
        return redirect()->route('customers.index')->withStatus(__('Cliente aggiornato con successo.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return void
     */
    public function destroy(User $customer)
    {
        if($customer->avatar != 'user.jpg') {
            File::delete('img/users/'.$customer->avatar);
        }
        if($customer->Cards()->exists()){
            foreach($customer->Cards() as $card){
                Card::destroy($card);
            }
        }
        $customer->delete();

        return redirect()->route('customers.index')->withStatus(__('Cliente eliminato con successo.'));
    }

    /**
     * Get filtered customer list
     *
     * @param Request $request
     */
    public function search(Request $request){

        $search = $request->search;
        $users = User::with('cards')
            ->where(function ($query) {
                $query->where('admin',0);
            })
            ->where(function ($query) use ($search) {
                $query->where('name', 'LIKE', '%'.$search.'%')
                    ->orWhere('surname', 'LIKE', '%'.$search.'%')
                    ->orwhereHas('cards', function($query) use($search) {
                        $query->where('number', 'LIKE', '%'.$search.'%');
                    });
            })
            ->paginate(15);

        return response()->json(['titolo'=>null,'tipo'=>'table','messaggio'=>null,'risultato'=>view('customers.table', compact('users'))->render()]);
    }

    /**
     * Search from home widget
     *
     */
    public function searchFromHome(Request $request) {
        {
            $search = $request->search;
            $users = User::with('cards')
                ->where(function ($query) {
                    $query->where('admin',0);
                })
                ->where(function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%'.$search.'%')
                        ->orWhere('surname', 'LIKE', '%'.$search.'%')
                        ->orwhereHas('cards', function($query) use($search) {
                          $query->where('number', 'LIKE', '%'.$search.'%');
                        });
                })
                ->paginate(15);
            return view('customers.index', ['users' => $users->paginate(15), 'search' => $search]);
        }
    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(CustomerPasswordRequest $request, User $customer)
    {
        $customer->update(['password' => Hash::make($request->get('password'))]);

        return back()->withStatusPassword(__('Password modificata con successo'));
    }

}
