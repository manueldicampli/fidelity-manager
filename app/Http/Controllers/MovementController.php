<?php

namespace App\Http\Controllers;

use App\Card;
use App\Movement;
use App\Notifications\MovementNotification;
use App\Operation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MovementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Movement $model)
    {
        $movements = $model->paginate(15);
        return view('movements.index', compact('movements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $operation = DB::table('operations')
            ->where('id', $request->operation_id)
            ->first();

        $amount = $request->amount * $operation->ppeuro;

        $movement = Movement::create([
            'type' => $request->type,
            'amount' => $amount,
            'location' => auth()->user()->company,
            'card_id' => $request->card_id,
            'operation_id' => $request->operation_id,
            'user_id' => auth()->user()->id
        ]);

        $card = Card::find($request->card_id);
        if($request->type == 'up'){
            $points = $card->points + $amount;
        }else {
            $points = $card->points - $amount;
        }

        $card->update([
            'points' => $points
        ]);
        $card->save();

        $customer = User::find($request->customer_id);
        $operations = Operation::all();

        $cards = $customer->cards;

        $points_to_prize = null;

        if($customer->email != null) {
            $customer->notify(new MovementNotification($movement, null, 'Nuovo movimento Fidelity | Cozzolino Hotels'));
        }

        return response()->json(['titolo'=>null, 'request' => $request, 'tipo'=>'modal','messaggio'=>'Operazione effettuata con successo!','risultato'=>view('customers.modal', ['customer' => $customer, 'cards' => $cards, 'operations' => $operations])->render()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movement $movement)
    {
        $card = $movement -> card;
        $movement_amount = $movement->amount;
        $card_points = $card->points;

        if($movement->type == 'up'){
            $card_points = $card_points - $movement_amount;
        }
        else {
            $card_points = $card_points + $movement_amount;
        }

        $card->update([
            'points' => $card_points
        ]);
        $card->save();

        $movement->delete();
        return redirect()->route('movement.index')->withStatus(__('Movimento eliminato con successo!'));
    }
}
