<?php

namespace App\Http\Controllers;

use App\Card;
use App\Coupon;
use App\Movement;
use App\Notifications\MovementNotification;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    /**
     * Display a prize
     *
     */
    public function showCoupon($slug)
    {
        $coupon = Coupon::where('slug', $slug)->firstOrFail();
        if ($coupon->Category) {
            $category_name = $coupon->category->name;
        } else {
            $category_name = 'Uncategorized';
        }

        $user_points = 0;

        foreach (Auth::user()->Cards()->get() as $card) {
            $user_points += $card->points;
        }

        $now = date('d/m/Y');

        $related_coupons = Coupon::where([
            ['id', '!=', $coupon->id],
            ['availability', '=', 'available'],
            ['deadline', '>', $now]
        ])->get();


        $title = $coupon->title . ' - ' . $category_name . ' | ' . $coupon->location;

        return view('frontend.coupon-show', ['coupon' => $coupon, 'category_name' => $category_name, 'related_coupons' => $related_coupons, 'user_points' => $user_points, 'title' => $title]);
    }

    /**
     * Prize request
     *
     * @return Response
     */
    public function requestPrize(Coupon $coupon, Request $request)
    {
        Auth::user()->Coupons()->attach($coupon->id);

        $prizeValue = $coupon->value;
        $user_points = 0;

        if (count(Auth::user()->cards) === 1) {
            $card = Auth::user()->cards->first();
            if ($card->points >= $prizeValue) {
                $card->update(['points' => $card->points - $prizeValue]);
                $card->save();

                $movement = Movement::create([
                    'type' => 'down',
                    'amount' => $prizeValue,
                    'location' => $coupon->location,
                    'card_id' => $card->id,
                    'coupon_id' => $coupon->id,
                    'operation_id' => 5,
                    'user_id' => auth()->user()->id
                ]);

                $user_points = $card->points;

                if(Auth::user()->email != null) {
                    $subject = 'Nuovo movimento Fidelity | Cozzolino Hotels';
                    if($movement->operation->id == 5) {
                        $subject = 'Informazioni in merito alla richiesta del suo premio | Cozzolino Hotels';
                    }
                    // MOVEMENT NOTIFICATION
                    Auth::user()->notify(new MovementNotification($movement, $coupon, $subject));
                }

            }
        } else {
            foreach ($request->data as $card) {
                $cardObj = Card::where('number', $card['number']);
                $cardBeforeMovements = $cardObj->first();
                $cardObj->update([
                    'points' => $card['points']
                ]);

                if(count($request->data) > 1) {
                    $isPartial = true;
                } else {
                    $isPartial = NULL;
                }

                $user_points += $card['points'];

                $movement_amount = strval($cardBeforeMovements->points - $card['points']);

                if($movement_amount > 0) {
                    Movement::create([
                        'type' => 'down',
                        'amount' => $movement_amount,
                        'location' => $coupon->location,
                        'partial' => $isPartial,
                        'card_id' => $cardBeforeMovements->id,
                        'coupon_id' => $coupon->id,
                        'operation_id' => 5,
                        'user_id' => auth()->user()->id
                    ]);
                }

            }
        }

        return response()->json(['messaggio' => '<strong>Richiesta effettuata con successo!</strong><br><br>Se hai fornito un indirizzo email a breve 
        riceverai tutte le info per la prenotazione del tuo premio, altrimenti <a class="btn-link" href="'.route('frontend.prize-status', $coupon->slug).'">clicca qui</a>.', 'tipo' => 'alert', 'userPoints' => $user_points, 'request' => $request->data]);
    }

    public function allMovements() {

        $user_points = 0;
        foreach (Auth::user()->Cards()->get() as $card) {
            $user_points += $card->points;
        }

        $movements = Auth::user()->CustomerMovements->sortByDesc('updated_at')->paginate(10);

        return view('frontend.movements', compact('movements', 'user_points'));
    }
    
    public function prizeStatus($slug) {

        $prize = Coupon::where('slug', $slug)->firstOrFail();

        $prize_request_status = Auth::user()->CouponPivot($prize->id);

        $user_points = 0;

        foreach (Auth::user()->Cards()->get() as $card) {
            $user_points += $card->points;
        }

        return view('frontend.prize-status', compact('prize_request_status', 'prize', 'user_points'));
    }

}
