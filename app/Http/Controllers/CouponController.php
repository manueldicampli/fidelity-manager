<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\CouponCategory;
use Illuminate\Support\Facades\File;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use App\Http\Requests\CouponRequest;
use App\Http\Requests\CouponSearchRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Coupon $model
     * @param Request $request
     * @return \Illuminate\View\View
     * @throws \Throwable
     */
    public function index(Coupon $model, Request $request)
    {
        $categories = CouponCategory::all();

        if ($request->ajax()) {
            return response()->json(['titolo'=>null,'tipo'=>null,'messaggio'=>null,'risultato'=>view('coupon.content', ['coupons' => $model->paginate(6), 'categories' => $categories])->render()]);
        }

        return view('coupon.index', ['coupons' => $model->paginate(6), 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = CouponCategory::all();

        return view('coupon.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CouponRequest  $request
     * @param  \App\Coupon  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CouponRequest $request, Coupon $model)
    {
        $slug = Str::slug($request->title, '-');

        $deadline = Carbon::createFromFormat('d/m/Y', $request->deadline)->format('Y-m-d');
    

        if($request->img !== 'coupon_img.png'){
            $image = $request->img;
            $name = time().'.png';
            $path = 'public/images/coupons/' . $name;

            list($type, $image) = explode(';', $image);
            list(, $image)      = explode(',', $image);
            $image = base64_decode($image);

            $img = Image::make($image);

            $img->stream();

            Storage::disk('local')->put($path, $img);

            $model->create($request->merge(['slug' => $slug, 'img' => $name, 'deadline' => $deadline])->all());

        } else {
            $model->create($request->merge(['slug' => $slug, 'deadline' => $deadline])->all());
        }

        return redirect()->route('coupon.index')->withStatus(__('Coupon creato con successo.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        if($coupon->Category){
           $category_name = $coupon->category->name; 
        } else {
            $category_name = 'Uncategorized';
        }
        return view('coupon.show', compact('coupon', 'category_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\View\View
     */
    public function edit(Coupon $coupon)
    {
        $categories = CouponCategory::all();

        return view('coupon.edit', compact('coupon', 'categories'));
    }


    /**
     * Update the specified coupon in storage.
     *
     * @param Request $request
     * @param Coupon $coupon
     * @return mixed
     */
    public function update(Request $request, Coupon $coupon)
    {
        if($request->img != $coupon->img && $request->img != 'coupon_img.png'){

            Storage::delete('public/images/coupons/'.$coupon->img);

            $image = $request->img;
            $name = time().'.png';
            $path = 'public/images/coupons/' . $name;

            list($type, $image) = explode(';', $image);
            list(, $image)      = explode(',', $image);
            $image = base64_decode($image);

            $img = Image::make($image);

            $img->stream();

            Storage::disk('local')->put($path, $img);

            $coupon->update([
                'img' => $name
            ]);

            $coupon->save();
        };

        $deadline = Carbon::createFromFormat('d/m/Y', $request->deadline)->format('Y-m-d');

        $coupon->update([
            'title' => $request->title,
            'description' => $request->description,
            'location' => $request->location,
            'deadline' => $deadline,
            'value' => $request->value,
            'availability' => $request->availability,
            'category_id' => $request->category_id
        ]);

        $coupon->save();

        return redirect()->route('coupon.index')->withStatus(__('Coupon aggiornato con successo.'));
    }

    /**
     * Remove the specified coupon from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Coupon $coupon)
    {
        if($coupon->img != 'coupon_img.png') {
            Storage::delete('public/images/coupons/'.$coupon->img);
        }
        $coupon->delete();

        return redirect()->route('coupon.index')->withStatus(__('Premio eliminato con successo!'));
    }

    /**
     * Search method.
     */
    public function search(Request $request) {

        if($request->category){
            $categories = array();
            foreach($request->category as $category){
                    $categories[] = $category;
            }
        }

        if($request->location) {
            $locations = array();
            foreach($request->location as $location){
                $locations[] = $location;
            }
        }
        

        if($request->category && $request->location) {
            $coupons = Coupon::WhereIn('category_id', $categories, 'and')->WhereIn('location', $locations, 'and')->orderBy('location')->paginate(5);
        }
        elseif($request->location) {
            $coupons = Coupon::WhereIn('location', $locations)->orderBy('location')->paginate(5);
        }
        elseif($request->category) {
            $coupons = Coupon::WhereIn('category_id', $categories)->orderBy('category_id')->paginate(5);
        }
        else {
            $coupons = Coupon::Where('title', 'LIKE', '%'.$request->search.'%')
            ->orWhere('description', 'LIKE', '%'.$request->search.'%')
            ->paginate(5);
        }
        $categories = CouponCategory::all();
        return response()->json(['titolo'=>null,'tipo'=>null,'messaggio'=>null,'risultato'=>view('coupon.content', compact('coupons','categories'))->render()]);
    }

    /**
     * Image crop method
     */

    public function imageCrop(Request $request){
        $image_file = $request->image;
        return response()->json(['status'=>true, 'img' => $image_file]);
      }


      public function convalidaPremio(Coupon $coupon) {
        $coupon->update([
            'accepted' => 'true'
        ]);

        $coupon->save();
          return response()->json(['titolo'=>null,'tipo'=>null,'messaggio'=>null,'risultato'=>view('coupon.content', compact('coupons','categories'))->render()]);
      }

}
