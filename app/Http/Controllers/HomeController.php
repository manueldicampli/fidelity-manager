<?php

namespace App\Http\Controllers;
use App\Movement;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Coupon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if(!Auth::user()->isAdmin()) {
            $movements = Auth::user()->CustomerMovements->sortByDesc('updated_at')->slice(0, 5);

            $user_points = 0;

            foreach (Auth::user()->Cards()->get() as $card) {
                $user_points += $card->points;
            }

            $coupons = Coupon::where('availability', 'available')->get();

            return view('home', compact('movements', 'coupons', 'user_points'));
        } else {
            $movements = Movement::where('operation_id', 5)->get();
            return view('home', compact('movements'));
        }
    }

    public function validatePrize($user, $prize) {
        if(Auth::user()->isAdmin()) {
            $prizeObj = Coupon::find($prize);
            $userObj = User::find($user);
            $validation_code = self::getValidationCode();
            $userObj->Coupons()->updateExistingPivot($prizeObj, array('accepted' => $validation_code));
            return Response::json(['status' => 'success', 'validationCode' => $validation_code]);
        } else {
            return Response::json(['message' => 'Ci hai provato! :D']);
        }
    }

    public function getValidationCode() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}
