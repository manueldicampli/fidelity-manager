<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'ppeuro'
    ];

    /**
     * Movement - Operation relationship.
     */
    public function Movements() {
        return $this->hasMany(Movement::class);
    }
}
