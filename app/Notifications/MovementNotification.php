<?php

namespace App\Notifications;

use App\Movement;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MovementNotification extends Notification
{
    use Queueable;
    protected $movement;

    /**
     * Create a new notification instance.
     * @param Movement $movement
     */
    public function __construct(Movement $movement, $prize = null, $mail_subject)
    {
        $this->movement = $movement;
        $this->prize = $prize;
        $this->mail_subject = $mail_subject;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $movement_icon_src = '';
        $movement = $this->movement;
        $prize = $this->prize;
        $mail_subject = $this->mail_subject;
        $user_points = 0;

        switch($movement->operation->name){
            case 'PERNOTTAMENTO':
                $movement_icon_src = asset('img/icons/bed.png');
                break;
            case 'RISTORANTE':
                $movement_icon_src = asset('img/icons/restaurant.png');
                break;
            case 'BAR':
                $movement_icon_src = asset('img/icons/cafe.png');
                break;
            case 'BENESSERE E RELAX':
                $movement_icon_src = asset('img/icons/spa.png');
                break;
            case 'ALTRO':
                $movement_icon_src = asset('img/icons/other.png');
                break;
            case 'RICHIESTA PREMIO':
                $movement_icon_src = asset('img/icons/trophy.png');
                break;
        }

        foreach($movement->card->user->cards()->get() as $card) {
            $user_points += $card->points;
        }

        return (new MailMessage)
                    ->from('noreply@cozzolinohotels.com')
                    ->subject($mail_subject)
                    ->view('vendor.notifications.movement', ['movement' => $movement, 'prize' => $prize, 'movement_icon_src' => $movement_icon_src, 'user_points' => $user_points]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
