<?php

namespace App;

use App\User;
use App\CouponCategory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'description', 'location', 'deadline', 'value', 'availability', 'category_id', 'img'
    ];

    /**
     * Coupon users relationship.
     */
    public function Users() {
        return $this->belongsToMany(User::class)->withPivot('accepted');
    }

    /**
     * Coupon category relationship.
     */
    public function Category(){
        return $this->belongsTo(CouponCategory::class);
    }

}
