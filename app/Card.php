<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number', 'points', 'user_id'
    ];

    /**
     * Card - User relationship.
     */
    public function User() {
        return $this->belongsTo(User::class);
    }

    /**
     * Card - User relationship.
     */
    public function Movements() {
        return $this->hasMany(Movement::class);
    }
}
