<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'city', 'gender', 'phone', 'birthday', 'company', 'job', 'email', 'username', 'password', 'newsletter', 'avatar', 'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * User's coupons relationship.
     */
    public function Coupons() {
        return $this->belongsToMany(Coupon::class)->withTimestamps()->withPivot('accepted');
    }

    /**
     * User's coupon pivot obj.
     */
    public function CouponPivot($id) {
        foreach ($this->coupons as $coupon) {
            if($coupon->id === $id) {
                return $coupon->pivot;
            }
        }
    }
    
    /**
     * User's card relationship.
     */
    public function Cards() {
        return $this->hasMany(Card::class);
    }

    /**
     * User's movements relationship.
     */
    public function Movements() {
        return $this->hasMany(Movement::class);
    }

    /**
     * Customer's card movements.
     */
    public function CustomerMovements() {
        return $this->hasManyThrough(Movement::class, Card::class);
    }


    /**
     * Checks if User is Admin.
     */

    public function isAdmin() {
        return $this->admin ? 1 : 0;
    }
}
