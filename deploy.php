<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'fidelity');

// Project repository
set('repository', 'git@bitbucket.org:manueldicampli/fidelity-manager.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Keep only last 3 releases
set('keep_releases', 3);

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);


// Hosts

host('manueldicampli.it')
    ->set('deploy_path', '/var/www/cozzolinohotels.com')
    ->user('manuel');


// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

//before('deploy:symlink', 'artisan:migrate');

